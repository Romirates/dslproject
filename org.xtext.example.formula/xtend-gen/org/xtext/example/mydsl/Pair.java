package org.xtext.example.mydsl;

import org.xtext.example.mydsl.formula.Formula;

@SuppressWarnings("all")
public class Pair {
  public final Formula p;
  
  public final Formula c;
  
  public Pair(final Formula x, final Formula y) {
    this.p = x;
    this.c = y;
  }
}
