package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.FormulaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFormulaParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'false'", "'Test'", "'with'", "';'", "'AllSolver'", "'<=>'", "'=>'", "'or'", "'nor'", "'and'", "'nand'", "'!'", "'('", "')'", "'SAT4J'", "'minisat'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFormulaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFormulaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFormulaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFormula.g"; }


    	private FormulaGrammarAccess grammarAccess;

    	public void setGrammarAccess(FormulaGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSolve"
    // InternalFormula.g:53:1: entryRuleSolve : ruleSolve EOF ;
    public final void entryRuleSolve() throws RecognitionException {
        try {
            // InternalFormula.g:54:1: ( ruleSolve EOF )
            // InternalFormula.g:55:1: ruleSolve EOF
            {
             before(grammarAccess.getSolveRule()); 
            pushFollow(FOLLOW_1);
            ruleSolve();

            state._fsp--;

             after(grammarAccess.getSolveRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolve"


    // $ANTLR start "ruleSolve"
    // InternalFormula.g:62:1: ruleSolve : ( ( rule__Solve__Group__0 ) ) ;
    public final void ruleSolve() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:66:2: ( ( ( rule__Solve__Group__0 ) ) )
            // InternalFormula.g:67:2: ( ( rule__Solve__Group__0 ) )
            {
            // InternalFormula.g:67:2: ( ( rule__Solve__Group__0 ) )
            // InternalFormula.g:68:3: ( rule__Solve__Group__0 )
            {
             before(grammarAccess.getSolveAccess().getGroup()); 
            // InternalFormula.g:69:3: ( rule__Solve__Group__0 )
            // InternalFormula.g:69:4: rule__Solve__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Solve__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSolveAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolve"


    // $ANTLR start "entryRuleTest"
    // InternalFormula.g:78:1: entryRuleTest : ruleTest EOF ;
    public final void entryRuleTest() throws RecognitionException {
        try {
            // InternalFormula.g:79:1: ( ruleTest EOF )
            // InternalFormula.g:80:1: ruleTest EOF
            {
             before(grammarAccess.getTestRule()); 
            pushFollow(FOLLOW_1);
            ruleTest();

            state._fsp--;

             after(grammarAccess.getTestRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTest"


    // $ANTLR start "ruleTest"
    // InternalFormula.g:87:1: ruleTest : ( ( rule__Test__Group__0 ) ) ;
    public final void ruleTest() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:91:2: ( ( ( rule__Test__Group__0 ) ) )
            // InternalFormula.g:92:2: ( ( rule__Test__Group__0 ) )
            {
            // InternalFormula.g:92:2: ( ( rule__Test__Group__0 ) )
            // InternalFormula.g:93:3: ( rule__Test__Group__0 )
            {
             before(grammarAccess.getTestAccess().getGroup()); 
            // InternalFormula.g:94:3: ( rule__Test__Group__0 )
            // InternalFormula.g:94:4: rule__Test__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Test__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTestAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTest"


    // $ANTLR start "entryRuleAllSolver"
    // InternalFormula.g:103:1: entryRuleAllSolver : ruleAllSolver EOF ;
    public final void entryRuleAllSolver() throws RecognitionException {
        try {
            // InternalFormula.g:104:1: ( ruleAllSolver EOF )
            // InternalFormula.g:105:1: ruleAllSolver EOF
            {
             before(grammarAccess.getAllSolverRule()); 
            pushFollow(FOLLOW_1);
            ruleAllSolver();

            state._fsp--;

             after(grammarAccess.getAllSolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAllSolver"


    // $ANTLR start "ruleAllSolver"
    // InternalFormula.g:112:1: ruleAllSolver : ( ( rule__AllSolver__Group__0 ) ) ;
    public final void ruleAllSolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:116:2: ( ( ( rule__AllSolver__Group__0 ) ) )
            // InternalFormula.g:117:2: ( ( rule__AllSolver__Group__0 ) )
            {
            // InternalFormula.g:117:2: ( ( rule__AllSolver__Group__0 ) )
            // InternalFormula.g:118:3: ( rule__AllSolver__Group__0 )
            {
             before(grammarAccess.getAllSolverAccess().getGroup()); 
            // InternalFormula.g:119:3: ( rule__AllSolver__Group__0 )
            // InternalFormula.g:119:4: rule__AllSolver__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AllSolver__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAllSolverAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAllSolver"


    // $ANTLR start "entryRuleSatsolver"
    // InternalFormula.g:128:1: entryRuleSatsolver : ruleSatsolver EOF ;
    public final void entryRuleSatsolver() throws RecognitionException {
        try {
            // InternalFormula.g:129:1: ( ruleSatsolver EOF )
            // InternalFormula.g:130:1: ruleSatsolver EOF
            {
             before(grammarAccess.getSatsolverRule()); 
            pushFollow(FOLLOW_1);
            ruleSatsolver();

            state._fsp--;

             after(grammarAccess.getSatsolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSatsolver"


    // $ANTLR start "ruleSatsolver"
    // InternalFormula.g:137:1: ruleSatsolver : ( ( rule__Satsolver__Alternatives ) ) ;
    public final void ruleSatsolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:141:2: ( ( ( rule__Satsolver__Alternatives ) ) )
            // InternalFormula.g:142:2: ( ( rule__Satsolver__Alternatives ) )
            {
            // InternalFormula.g:142:2: ( ( rule__Satsolver__Alternatives ) )
            // InternalFormula.g:143:3: ( rule__Satsolver__Alternatives )
            {
             before(grammarAccess.getSatsolverAccess().getAlternatives()); 
            // InternalFormula.g:144:3: ( rule__Satsolver__Alternatives )
            // InternalFormula.g:144:4: rule__Satsolver__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Satsolver__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSatsolverAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSatsolver"


    // $ANTLR start "entryRuleEquiv"
    // InternalFormula.g:153:1: entryRuleEquiv : ruleEquiv EOF ;
    public final void entryRuleEquiv() throws RecognitionException {
        try {
            // InternalFormula.g:154:1: ( ruleEquiv EOF )
            // InternalFormula.g:155:1: ruleEquiv EOF
            {
             before(grammarAccess.getEquivRule()); 
            pushFollow(FOLLOW_1);
            ruleEquiv();

            state._fsp--;

             after(grammarAccess.getEquivRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEquiv"


    // $ANTLR start "ruleEquiv"
    // InternalFormula.g:162:1: ruleEquiv : ( ( rule__Equiv__Group__0 ) ) ;
    public final void ruleEquiv() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:166:2: ( ( ( rule__Equiv__Group__0 ) ) )
            // InternalFormula.g:167:2: ( ( rule__Equiv__Group__0 ) )
            {
            // InternalFormula.g:167:2: ( ( rule__Equiv__Group__0 ) )
            // InternalFormula.g:168:3: ( rule__Equiv__Group__0 )
            {
             before(grammarAccess.getEquivAccess().getGroup()); 
            // InternalFormula.g:169:3: ( rule__Equiv__Group__0 )
            // InternalFormula.g:169:4: rule__Equiv__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Equiv__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEquivAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEquiv"


    // $ANTLR start "entryRuleImplie"
    // InternalFormula.g:178:1: entryRuleImplie : ruleImplie EOF ;
    public final void entryRuleImplie() throws RecognitionException {
        try {
            // InternalFormula.g:179:1: ( ruleImplie EOF )
            // InternalFormula.g:180:1: ruleImplie EOF
            {
             before(grammarAccess.getImplieRule()); 
            pushFollow(FOLLOW_1);
            ruleImplie();

            state._fsp--;

             after(grammarAccess.getImplieRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplie"


    // $ANTLR start "ruleImplie"
    // InternalFormula.g:187:1: ruleImplie : ( ( rule__Implie__Group__0 ) ) ;
    public final void ruleImplie() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:191:2: ( ( ( rule__Implie__Group__0 ) ) )
            // InternalFormula.g:192:2: ( ( rule__Implie__Group__0 ) )
            {
            // InternalFormula.g:192:2: ( ( rule__Implie__Group__0 ) )
            // InternalFormula.g:193:3: ( rule__Implie__Group__0 )
            {
             before(grammarAccess.getImplieAccess().getGroup()); 
            // InternalFormula.g:194:3: ( rule__Implie__Group__0 )
            // InternalFormula.g:194:4: rule__Implie__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implie__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImplieAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplie"


    // $ANTLR start "entryRuleOr"
    // InternalFormula.g:203:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalFormula.g:204:1: ( ruleOr EOF )
            // InternalFormula.g:205:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalFormula.g:212:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:216:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalFormula.g:217:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalFormula.g:217:2: ( ( rule__Or__Group__0 ) )
            // InternalFormula.g:218:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalFormula.g:219:3: ( rule__Or__Group__0 )
            // InternalFormula.g:219:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleNor"
    // InternalFormula.g:228:1: entryRuleNor : ruleNor EOF ;
    public final void entryRuleNor() throws RecognitionException {
        try {
            // InternalFormula.g:229:1: ( ruleNor EOF )
            // InternalFormula.g:230:1: ruleNor EOF
            {
             before(grammarAccess.getNorRule()); 
            pushFollow(FOLLOW_1);
            ruleNor();

            state._fsp--;

             after(grammarAccess.getNorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNor"


    // $ANTLR start "ruleNor"
    // InternalFormula.g:237:1: ruleNor : ( ( rule__Nor__Group__0 ) ) ;
    public final void ruleNor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:241:2: ( ( ( rule__Nor__Group__0 ) ) )
            // InternalFormula.g:242:2: ( ( rule__Nor__Group__0 ) )
            {
            // InternalFormula.g:242:2: ( ( rule__Nor__Group__0 ) )
            // InternalFormula.g:243:3: ( rule__Nor__Group__0 )
            {
             before(grammarAccess.getNorAccess().getGroup()); 
            // InternalFormula.g:244:3: ( rule__Nor__Group__0 )
            // InternalFormula.g:244:4: rule__Nor__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Nor__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNor"


    // $ANTLR start "entryRuleAnd"
    // InternalFormula.g:253:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalFormula.g:254:1: ( ruleAnd EOF )
            // InternalFormula.g:255:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalFormula.g:262:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:266:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalFormula.g:267:2: ( ( rule__And__Group__0 ) )
            {
            // InternalFormula.g:267:2: ( ( rule__And__Group__0 ) )
            // InternalFormula.g:268:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalFormula.g:269:3: ( rule__And__Group__0 )
            // InternalFormula.g:269:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNand"
    // InternalFormula.g:278:1: entryRuleNand : ruleNand EOF ;
    public final void entryRuleNand() throws RecognitionException {
        try {
            // InternalFormula.g:279:1: ( ruleNand EOF )
            // InternalFormula.g:280:1: ruleNand EOF
            {
             before(grammarAccess.getNandRule()); 
            pushFollow(FOLLOW_1);
            ruleNand();

            state._fsp--;

             after(grammarAccess.getNandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNand"


    // $ANTLR start "ruleNand"
    // InternalFormula.g:287:1: ruleNand : ( ( rule__Nand__Group__0 ) ) ;
    public final void ruleNand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:291:2: ( ( ( rule__Nand__Group__0 ) ) )
            // InternalFormula.g:292:2: ( ( rule__Nand__Group__0 ) )
            {
            // InternalFormula.g:292:2: ( ( rule__Nand__Group__0 ) )
            // InternalFormula.g:293:3: ( rule__Nand__Group__0 )
            {
             before(grammarAccess.getNandAccess().getGroup()); 
            // InternalFormula.g:294:3: ( rule__Nand__Group__0 )
            // InternalFormula.g:294:4: rule__Nand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Nand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNand"


    // $ANTLR start "entryRuleNot"
    // InternalFormula.g:303:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalFormula.g:304:1: ( ruleNot EOF )
            // InternalFormula.g:305:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalFormula.g:312:1: ruleNot : ( ( rule__Not__Group__0 ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:316:2: ( ( ( rule__Not__Group__0 ) ) )
            // InternalFormula.g:317:2: ( ( rule__Not__Group__0 ) )
            {
            // InternalFormula.g:317:2: ( ( rule__Not__Group__0 ) )
            // InternalFormula.g:318:3: ( rule__Not__Group__0 )
            {
             before(grammarAccess.getNotAccess().getGroup()); 
            // InternalFormula.g:319:3: ( rule__Not__Group__0 )
            // InternalFormula.g:319:4: rule__Not__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalFormula.g:328:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalFormula.g:329:1: ( rulePrimary EOF )
            // InternalFormula.g:330:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalFormula.g:337:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:341:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalFormula.g:342:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalFormula.g:342:2: ( ( rule__Primary__Alternatives ) )
            // InternalFormula.g:343:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalFormula.g:344:3: ( rule__Primary__Alternatives )
            // InternalFormula.g:344:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // InternalFormula.g:353:1: entryRuleAtomic : ruleAtomic EOF ;
    public final void entryRuleAtomic() throws RecognitionException {
        try {
            // InternalFormula.g:354:1: ( ruleAtomic EOF )
            // InternalFormula.g:355:1: ruleAtomic EOF
            {
             before(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getAtomicRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalFormula.g:362:1: ruleAtomic : ( ( rule__Atomic__Alternatives ) ) ;
    public final void ruleAtomic() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:366:2: ( ( ( rule__Atomic__Alternatives ) ) )
            // InternalFormula.g:367:2: ( ( rule__Atomic__Alternatives ) )
            {
            // InternalFormula.g:367:2: ( ( rule__Atomic__Alternatives ) )
            // InternalFormula.g:368:3: ( rule__Atomic__Alternatives )
            {
             before(grammarAccess.getAtomicAccess().getAlternatives()); 
            // InternalFormula.g:369:3: ( rule__Atomic__Alternatives )
            // InternalFormula.g:369:4: rule__Atomic__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRulePFormula"
    // InternalFormula.g:378:1: entryRulePFormula : rulePFormula EOF ;
    public final void entryRulePFormula() throws RecognitionException {
        try {
            // InternalFormula.g:379:1: ( rulePFormula EOF )
            // InternalFormula.g:380:1: rulePFormula EOF
            {
             before(grammarAccess.getPFormulaRule()); 
            pushFollow(FOLLOW_1);
            rulePFormula();

            state._fsp--;

             after(grammarAccess.getPFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePFormula"


    // $ANTLR start "rulePFormula"
    // InternalFormula.g:387:1: rulePFormula : ( ( rule__PFormula__Group__0 ) ) ;
    public final void rulePFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:391:2: ( ( ( rule__PFormula__Group__0 ) ) )
            // InternalFormula.g:392:2: ( ( rule__PFormula__Group__0 ) )
            {
            // InternalFormula.g:392:2: ( ( rule__PFormula__Group__0 ) )
            // InternalFormula.g:393:3: ( rule__PFormula__Group__0 )
            {
             before(grammarAccess.getPFormulaAccess().getGroup()); 
            // InternalFormula.g:394:3: ( rule__PFormula__Group__0 )
            // InternalFormula.g:394:4: rule__PFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePFormula"


    // $ANTLR start "rule__Solve__Alternatives_0"
    // InternalFormula.g:402:1: rule__Solve__Alternatives_0 : ( ( ruleAllSolver ) | ( ruleTest ) );
    public final void rule__Solve__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:406:1: ( ( ruleAllSolver ) | ( ruleTest ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==16) ) {
                alt1=1;
            }
            else if ( (LA1_0==13) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalFormula.g:407:2: ( ruleAllSolver )
                    {
                    // InternalFormula.g:407:2: ( ruleAllSolver )
                    // InternalFormula.g:408:3: ruleAllSolver
                    {
                     before(grammarAccess.getSolveAccess().getAllSolverParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAllSolver();

                    state._fsp--;

                     after(grammarAccess.getSolveAccess().getAllSolverParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:413:2: ( ruleTest )
                    {
                    // InternalFormula.g:413:2: ( ruleTest )
                    // InternalFormula.g:414:3: ruleTest
                    {
                     before(grammarAccess.getSolveAccess().getTestParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleTest();

                    state._fsp--;

                     after(grammarAccess.getSolveAccess().getTestParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Alternatives_0"


    // $ANTLR start "rule__Solve__TestsAlternatives_2_0"
    // InternalFormula.g:423:1: rule__Solve__TestsAlternatives_2_0 : ( ( ruleTest ) | ( ruleAllSolver ) );
    public final void rule__Solve__TestsAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:427:1: ( ( ruleTest ) | ( ruleAllSolver ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( (LA2_0==16) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalFormula.g:428:2: ( ruleTest )
                    {
                    // InternalFormula.g:428:2: ( ruleTest )
                    // InternalFormula.g:429:3: ruleTest
                    {
                     before(grammarAccess.getSolveAccess().getTestsTestParserRuleCall_2_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTest();

                    state._fsp--;

                     after(grammarAccess.getSolveAccess().getTestsTestParserRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:434:2: ( ruleAllSolver )
                    {
                    // InternalFormula.g:434:2: ( ruleAllSolver )
                    // InternalFormula.g:435:3: ruleAllSolver
                    {
                     before(grammarAccess.getSolveAccess().getTestsAllSolverParserRuleCall_2_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAllSolver();

                    state._fsp--;

                     after(grammarAccess.getSolveAccess().getTestsAllSolverParserRuleCall_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__TestsAlternatives_2_0"


    // $ANTLR start "rule__Satsolver__Alternatives"
    // InternalFormula.g:444:1: rule__Satsolver__Alternatives : ( ( ( rule__Satsolver__NameAssignment_0 ) ) | ( ( rule__Satsolver__NameAssignment_1 ) ) );
    public final void rule__Satsolver__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:448:1: ( ( ( rule__Satsolver__NameAssignment_0 ) ) | ( ( rule__Satsolver__NameAssignment_1 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==26) ) {
                alt3=1;
            }
            else if ( (LA3_0==27) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalFormula.g:449:2: ( ( rule__Satsolver__NameAssignment_0 ) )
                    {
                    // InternalFormula.g:449:2: ( ( rule__Satsolver__NameAssignment_0 ) )
                    // InternalFormula.g:450:3: ( rule__Satsolver__NameAssignment_0 )
                    {
                     before(grammarAccess.getSatsolverAccess().getNameAssignment_0()); 
                    // InternalFormula.g:451:3: ( rule__Satsolver__NameAssignment_0 )
                    // InternalFormula.g:451:4: rule__Satsolver__NameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Satsolver__NameAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSatsolverAccess().getNameAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:455:2: ( ( rule__Satsolver__NameAssignment_1 ) )
                    {
                    // InternalFormula.g:455:2: ( ( rule__Satsolver__NameAssignment_1 ) )
                    // InternalFormula.g:456:3: ( rule__Satsolver__NameAssignment_1 )
                    {
                     before(grammarAccess.getSatsolverAccess().getNameAssignment_1()); 
                    // InternalFormula.g:457:3: ( rule__Satsolver__NameAssignment_1 )
                    // InternalFormula.g:457:4: rule__Satsolver__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Satsolver__NameAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSatsolverAccess().getNameAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Satsolver__Alternatives"


    // $ANTLR start "rule__Nand__Alternatives_0"
    // InternalFormula.g:465:1: rule__Nand__Alternatives_0 : ( ( ruleNot ) | ( rulePrimary ) );
    public final void rule__Nand__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:469:1: ( ( ruleNot ) | ( rulePrimary ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID||(LA4_0>=11 && LA4_0<=12)||LA4_0==24) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalFormula.g:470:2: ( ruleNot )
                    {
                    // InternalFormula.g:470:2: ( ruleNot )
                    // InternalFormula.g:471:3: ruleNot
                    {
                     before(grammarAccess.getNandAccess().getNotParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleNot();

                    state._fsp--;

                     after(grammarAccess.getNandAccess().getNotParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:476:2: ( rulePrimary )
                    {
                    // InternalFormula.g:476:2: ( rulePrimary )
                    // InternalFormula.g:477:3: rulePrimary
                    {
                     before(grammarAccess.getNandAccess().getPrimaryParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getNandAccess().getPrimaryParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Alternatives_0"


    // $ANTLR start "rule__Nand__RightAlternatives_1_2_0"
    // InternalFormula.g:486:1: rule__Nand__RightAlternatives_1_2_0 : ( ( ruleNot ) | ( rulePrimary ) );
    public final void rule__Nand__RightAlternatives_1_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:490:1: ( ( ruleNot ) | ( rulePrimary ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==23) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_ID||(LA5_0>=11 && LA5_0<=12)||LA5_0==24) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalFormula.g:491:2: ( ruleNot )
                    {
                    // InternalFormula.g:491:2: ( ruleNot )
                    // InternalFormula.g:492:3: ruleNot
                    {
                     before(grammarAccess.getNandAccess().getRightNotParserRuleCall_1_2_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleNot();

                    state._fsp--;

                     after(grammarAccess.getNandAccess().getRightNotParserRuleCall_1_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:497:2: ( rulePrimary )
                    {
                    // InternalFormula.g:497:2: ( rulePrimary )
                    // InternalFormula.g:498:3: rulePrimary
                    {
                     before(grammarAccess.getNandAccess().getRightPrimaryParserRuleCall_1_2_0_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getNandAccess().getRightPrimaryParserRuleCall_1_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__RightAlternatives_1_2_0"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalFormula.g:507:1: rule__Primary__Alternatives : ( ( ruleAtomic ) | ( rulePFormula ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:511:1: ( ( ruleAtomic ) | ( rulePFormula ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID||(LA6_0>=11 && LA6_0<=12)) ) {
                alt6=1;
            }
            else if ( (LA6_0==24) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalFormula.g:512:2: ( ruleAtomic )
                    {
                    // InternalFormula.g:512:2: ( ruleAtomic )
                    // InternalFormula.g:513:3: ruleAtomic
                    {
                     before(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAtomic();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:518:2: ( rulePFormula )
                    {
                    // InternalFormula.g:518:2: ( rulePFormula )
                    // InternalFormula.g:519:3: rulePFormula
                    {
                     before(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePFormula();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Atomic__Alternatives"
    // InternalFormula.g:528:1: rule__Atomic__Alternatives : ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) );
    public final void rule__Atomic__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:532:1: ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=11 && LA7_0<=12)) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalFormula.g:533:2: ( ( rule__Atomic__Group_0__0 ) )
                    {
                    // InternalFormula.g:533:2: ( ( rule__Atomic__Group_0__0 ) )
                    // InternalFormula.g:534:3: ( rule__Atomic__Group_0__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_0()); 
                    // InternalFormula.g:535:3: ( rule__Atomic__Group_0__0 )
                    // InternalFormula.g:535:4: rule__Atomic__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:539:2: ( ( rule__Atomic__Group_1__0 ) )
                    {
                    // InternalFormula.g:539:2: ( ( rule__Atomic__Group_1__0 ) )
                    // InternalFormula.g:540:3: ( rule__Atomic__Group_1__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_1()); 
                    // InternalFormula.g:541:3: ( rule__Atomic__Group_1__0 )
                    // InternalFormula.g:541:4: rule__Atomic__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Alternatives"


    // $ANTLR start "rule__Atomic__ValueAlternatives_1_1_0"
    // InternalFormula.g:549:1: rule__Atomic__ValueAlternatives_1_1_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__Atomic__ValueAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:553:1: ( ( 'true' ) | ( 'false' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==11) ) {
                alt8=1;
            }
            else if ( (LA8_0==12) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalFormula.g:554:2: ( 'true' )
                    {
                    // InternalFormula.g:554:2: ( 'true' )
                    // InternalFormula.g:555:3: 'true'
                    {
                     before(grammarAccess.getAtomicAccess().getValueTrueKeyword_1_1_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAtomicAccess().getValueTrueKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:560:2: ( 'false' )
                    {
                    // InternalFormula.g:560:2: ( 'false' )
                    // InternalFormula.g:561:3: 'false'
                    {
                     before(grammarAccess.getAtomicAccess().getValueFalseKeyword_1_1_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAtomicAccess().getValueFalseKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAlternatives_1_1_0"


    // $ANTLR start "rule__Solve__Group__0"
    // InternalFormula.g:570:1: rule__Solve__Group__0 : rule__Solve__Group__0__Impl rule__Solve__Group__1 ;
    public final void rule__Solve__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:574:1: ( rule__Solve__Group__0__Impl rule__Solve__Group__1 )
            // InternalFormula.g:575:2: rule__Solve__Group__0__Impl rule__Solve__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Solve__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solve__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__0"


    // $ANTLR start "rule__Solve__Group__0__Impl"
    // InternalFormula.g:582:1: rule__Solve__Group__0__Impl : ( ( rule__Solve__Alternatives_0 ) ) ;
    public final void rule__Solve__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:586:1: ( ( ( rule__Solve__Alternatives_0 ) ) )
            // InternalFormula.g:587:1: ( ( rule__Solve__Alternatives_0 ) )
            {
            // InternalFormula.g:587:1: ( ( rule__Solve__Alternatives_0 ) )
            // InternalFormula.g:588:2: ( rule__Solve__Alternatives_0 )
            {
             before(grammarAccess.getSolveAccess().getAlternatives_0()); 
            // InternalFormula.g:589:2: ( rule__Solve__Alternatives_0 )
            // InternalFormula.g:589:3: rule__Solve__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Solve__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getSolveAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__0__Impl"


    // $ANTLR start "rule__Solve__Group__1"
    // InternalFormula.g:597:1: rule__Solve__Group__1 : rule__Solve__Group__1__Impl rule__Solve__Group__2 ;
    public final void rule__Solve__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:601:1: ( rule__Solve__Group__1__Impl rule__Solve__Group__2 )
            // InternalFormula.g:602:2: rule__Solve__Group__1__Impl rule__Solve__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Solve__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solve__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__1"


    // $ANTLR start "rule__Solve__Group__1__Impl"
    // InternalFormula.g:609:1: rule__Solve__Group__1__Impl : ( () ) ;
    public final void rule__Solve__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:613:1: ( ( () ) )
            // InternalFormula.g:614:1: ( () )
            {
            // InternalFormula.g:614:1: ( () )
            // InternalFormula.g:615:2: ()
            {
             before(grammarAccess.getSolveAccess().getSolveTestsAction_1()); 
            // InternalFormula.g:616:2: ()
            // InternalFormula.g:616:3: 
            {
            }

             after(grammarAccess.getSolveAccess().getSolveTestsAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__1__Impl"


    // $ANTLR start "rule__Solve__Group__2"
    // InternalFormula.g:624:1: rule__Solve__Group__2 : rule__Solve__Group__2__Impl ;
    public final void rule__Solve__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:628:1: ( rule__Solve__Group__2__Impl )
            // InternalFormula.g:629:2: rule__Solve__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solve__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__2"


    // $ANTLR start "rule__Solve__Group__2__Impl"
    // InternalFormula.g:635:1: rule__Solve__Group__2__Impl : ( ( rule__Solve__TestsAssignment_2 )* ) ;
    public final void rule__Solve__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:639:1: ( ( ( rule__Solve__TestsAssignment_2 )* ) )
            // InternalFormula.g:640:1: ( ( rule__Solve__TestsAssignment_2 )* )
            {
            // InternalFormula.g:640:1: ( ( rule__Solve__TestsAssignment_2 )* )
            // InternalFormula.g:641:2: ( rule__Solve__TestsAssignment_2 )*
            {
             before(grammarAccess.getSolveAccess().getTestsAssignment_2()); 
            // InternalFormula.g:642:2: ( rule__Solve__TestsAssignment_2 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==13||LA9_0==16) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalFormula.g:642:3: rule__Solve__TestsAssignment_2
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Solve__TestsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getSolveAccess().getTestsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__2__Impl"


    // $ANTLR start "rule__Test__Group__0"
    // InternalFormula.g:651:1: rule__Test__Group__0 : rule__Test__Group__0__Impl rule__Test__Group__1 ;
    public final void rule__Test__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:655:1: ( rule__Test__Group__0__Impl rule__Test__Group__1 )
            // InternalFormula.g:656:2: rule__Test__Group__0__Impl rule__Test__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Test__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Test__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__0"


    // $ANTLR start "rule__Test__Group__0__Impl"
    // InternalFormula.g:663:1: rule__Test__Group__0__Impl : ( 'Test' ) ;
    public final void rule__Test__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:667:1: ( ( 'Test' ) )
            // InternalFormula.g:668:1: ( 'Test' )
            {
            // InternalFormula.g:668:1: ( 'Test' )
            // InternalFormula.g:669:2: 'Test'
            {
             before(grammarAccess.getTestAccess().getTestKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getTestAccess().getTestKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__0__Impl"


    // $ANTLR start "rule__Test__Group__1"
    // InternalFormula.g:678:1: rule__Test__Group__1 : rule__Test__Group__1__Impl rule__Test__Group__2 ;
    public final void rule__Test__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:682:1: ( rule__Test__Group__1__Impl rule__Test__Group__2 )
            // InternalFormula.g:683:2: rule__Test__Group__1__Impl rule__Test__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Test__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Test__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__1"


    // $ANTLR start "rule__Test__Group__1__Impl"
    // InternalFormula.g:690:1: rule__Test__Group__1__Impl : ( ( rule__Test__FormulaAssignment_1 ) ) ;
    public final void rule__Test__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:694:1: ( ( ( rule__Test__FormulaAssignment_1 ) ) )
            // InternalFormula.g:695:1: ( ( rule__Test__FormulaAssignment_1 ) )
            {
            // InternalFormula.g:695:1: ( ( rule__Test__FormulaAssignment_1 ) )
            // InternalFormula.g:696:2: ( rule__Test__FormulaAssignment_1 )
            {
             before(grammarAccess.getTestAccess().getFormulaAssignment_1()); 
            // InternalFormula.g:697:2: ( rule__Test__FormulaAssignment_1 )
            // InternalFormula.g:697:3: rule__Test__FormulaAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Test__FormulaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTestAccess().getFormulaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__1__Impl"


    // $ANTLR start "rule__Test__Group__2"
    // InternalFormula.g:705:1: rule__Test__Group__2 : rule__Test__Group__2__Impl rule__Test__Group__3 ;
    public final void rule__Test__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:709:1: ( rule__Test__Group__2__Impl rule__Test__Group__3 )
            // InternalFormula.g:710:2: rule__Test__Group__2__Impl rule__Test__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Test__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Test__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__2"


    // $ANTLR start "rule__Test__Group__2__Impl"
    // InternalFormula.g:717:1: rule__Test__Group__2__Impl : ( 'with' ) ;
    public final void rule__Test__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:721:1: ( ( 'with' ) )
            // InternalFormula.g:722:1: ( 'with' )
            {
            // InternalFormula.g:722:1: ( 'with' )
            // InternalFormula.g:723:2: 'with'
            {
             before(grammarAccess.getTestAccess().getWithKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTestAccess().getWithKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__2__Impl"


    // $ANTLR start "rule__Test__Group__3"
    // InternalFormula.g:732:1: rule__Test__Group__3 : rule__Test__Group__3__Impl rule__Test__Group__4 ;
    public final void rule__Test__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:736:1: ( rule__Test__Group__3__Impl rule__Test__Group__4 )
            // InternalFormula.g:737:2: rule__Test__Group__3__Impl rule__Test__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Test__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Test__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__3"


    // $ANTLR start "rule__Test__Group__3__Impl"
    // InternalFormula.g:744:1: rule__Test__Group__3__Impl : ( ( rule__Test__SolverAssignment_3 ) ) ;
    public final void rule__Test__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:748:1: ( ( ( rule__Test__SolverAssignment_3 ) ) )
            // InternalFormula.g:749:1: ( ( rule__Test__SolverAssignment_3 ) )
            {
            // InternalFormula.g:749:1: ( ( rule__Test__SolverAssignment_3 ) )
            // InternalFormula.g:750:2: ( rule__Test__SolverAssignment_3 )
            {
             before(grammarAccess.getTestAccess().getSolverAssignment_3()); 
            // InternalFormula.g:751:2: ( rule__Test__SolverAssignment_3 )
            // InternalFormula.g:751:3: rule__Test__SolverAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Test__SolverAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTestAccess().getSolverAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__3__Impl"


    // $ANTLR start "rule__Test__Group__4"
    // InternalFormula.g:759:1: rule__Test__Group__4 : rule__Test__Group__4__Impl ;
    public final void rule__Test__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:763:1: ( rule__Test__Group__4__Impl )
            // InternalFormula.g:764:2: rule__Test__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Test__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__4"


    // $ANTLR start "rule__Test__Group__4__Impl"
    // InternalFormula.g:770:1: rule__Test__Group__4__Impl : ( ';' ) ;
    public final void rule__Test__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:774:1: ( ( ';' ) )
            // InternalFormula.g:775:1: ( ';' )
            {
            // InternalFormula.g:775:1: ( ';' )
            // InternalFormula.g:776:2: ';'
            {
             before(grammarAccess.getTestAccess().getSemicolonKeyword_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getTestAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__Group__4__Impl"


    // $ANTLR start "rule__AllSolver__Group__0"
    // InternalFormula.g:786:1: rule__AllSolver__Group__0 : rule__AllSolver__Group__0__Impl rule__AllSolver__Group__1 ;
    public final void rule__AllSolver__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:790:1: ( rule__AllSolver__Group__0__Impl rule__AllSolver__Group__1 )
            // InternalFormula.g:791:2: rule__AllSolver__Group__0__Impl rule__AllSolver__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__AllSolver__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AllSolver__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__0"


    // $ANTLR start "rule__AllSolver__Group__0__Impl"
    // InternalFormula.g:798:1: rule__AllSolver__Group__0__Impl : ( 'AllSolver' ) ;
    public final void rule__AllSolver__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:802:1: ( ( 'AllSolver' ) )
            // InternalFormula.g:803:1: ( 'AllSolver' )
            {
            // InternalFormula.g:803:1: ( 'AllSolver' )
            // InternalFormula.g:804:2: 'AllSolver'
            {
             before(grammarAccess.getAllSolverAccess().getAllSolverKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAllSolverAccess().getAllSolverKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__0__Impl"


    // $ANTLR start "rule__AllSolver__Group__1"
    // InternalFormula.g:813:1: rule__AllSolver__Group__1 : rule__AllSolver__Group__1__Impl rule__AllSolver__Group__2 ;
    public final void rule__AllSolver__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:817:1: ( rule__AllSolver__Group__1__Impl rule__AllSolver__Group__2 )
            // InternalFormula.g:818:2: rule__AllSolver__Group__1__Impl rule__AllSolver__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__AllSolver__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AllSolver__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__1"


    // $ANTLR start "rule__AllSolver__Group__1__Impl"
    // InternalFormula.g:825:1: rule__AllSolver__Group__1__Impl : ( ( rule__AllSolver__FormulaAssignment_1 ) ) ;
    public final void rule__AllSolver__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:829:1: ( ( ( rule__AllSolver__FormulaAssignment_1 ) ) )
            // InternalFormula.g:830:1: ( ( rule__AllSolver__FormulaAssignment_1 ) )
            {
            // InternalFormula.g:830:1: ( ( rule__AllSolver__FormulaAssignment_1 ) )
            // InternalFormula.g:831:2: ( rule__AllSolver__FormulaAssignment_1 )
            {
             before(grammarAccess.getAllSolverAccess().getFormulaAssignment_1()); 
            // InternalFormula.g:832:2: ( rule__AllSolver__FormulaAssignment_1 )
            // InternalFormula.g:832:3: rule__AllSolver__FormulaAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AllSolver__FormulaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAllSolverAccess().getFormulaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__1__Impl"


    // $ANTLR start "rule__AllSolver__Group__2"
    // InternalFormula.g:840:1: rule__AllSolver__Group__2 : rule__AllSolver__Group__2__Impl ;
    public final void rule__AllSolver__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:844:1: ( rule__AllSolver__Group__2__Impl )
            // InternalFormula.g:845:2: rule__AllSolver__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AllSolver__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__2"


    // $ANTLR start "rule__AllSolver__Group__2__Impl"
    // InternalFormula.g:851:1: rule__AllSolver__Group__2__Impl : ( ';' ) ;
    public final void rule__AllSolver__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:855:1: ( ( ';' ) )
            // InternalFormula.g:856:1: ( ';' )
            {
            // InternalFormula.g:856:1: ( ';' )
            // InternalFormula.g:857:2: ';'
            {
             before(grammarAccess.getAllSolverAccess().getSemicolonKeyword_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getAllSolverAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__Group__2__Impl"


    // $ANTLR start "rule__Equiv__Group__0"
    // InternalFormula.g:867:1: rule__Equiv__Group__0 : rule__Equiv__Group__0__Impl rule__Equiv__Group__1 ;
    public final void rule__Equiv__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:871:1: ( rule__Equiv__Group__0__Impl rule__Equiv__Group__1 )
            // InternalFormula.g:872:2: rule__Equiv__Group__0__Impl rule__Equiv__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Equiv__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equiv__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group__0"


    // $ANTLR start "rule__Equiv__Group__0__Impl"
    // InternalFormula.g:879:1: rule__Equiv__Group__0__Impl : ( ruleImplie ) ;
    public final void rule__Equiv__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:883:1: ( ( ruleImplie ) )
            // InternalFormula.g:884:1: ( ruleImplie )
            {
            // InternalFormula.g:884:1: ( ruleImplie )
            // InternalFormula.g:885:2: ruleImplie
            {
             before(grammarAccess.getEquivAccess().getImplieParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplie();

            state._fsp--;

             after(grammarAccess.getEquivAccess().getImplieParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group__0__Impl"


    // $ANTLR start "rule__Equiv__Group__1"
    // InternalFormula.g:894:1: rule__Equiv__Group__1 : rule__Equiv__Group__1__Impl ;
    public final void rule__Equiv__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:898:1: ( rule__Equiv__Group__1__Impl )
            // InternalFormula.g:899:2: rule__Equiv__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equiv__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group__1"


    // $ANTLR start "rule__Equiv__Group__1__Impl"
    // InternalFormula.g:905:1: rule__Equiv__Group__1__Impl : ( ( rule__Equiv__Group_1__0 )* ) ;
    public final void rule__Equiv__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:909:1: ( ( ( rule__Equiv__Group_1__0 )* ) )
            // InternalFormula.g:910:1: ( ( rule__Equiv__Group_1__0 )* )
            {
            // InternalFormula.g:910:1: ( ( rule__Equiv__Group_1__0 )* )
            // InternalFormula.g:911:2: ( rule__Equiv__Group_1__0 )*
            {
             before(grammarAccess.getEquivAccess().getGroup_1()); 
            // InternalFormula.g:912:2: ( rule__Equiv__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==17) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalFormula.g:912:3: rule__Equiv__Group_1__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Equiv__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getEquivAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group__1__Impl"


    // $ANTLR start "rule__Equiv__Group_1__0"
    // InternalFormula.g:921:1: rule__Equiv__Group_1__0 : rule__Equiv__Group_1__0__Impl rule__Equiv__Group_1__1 ;
    public final void rule__Equiv__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:925:1: ( rule__Equiv__Group_1__0__Impl rule__Equiv__Group_1__1 )
            // InternalFormula.g:926:2: rule__Equiv__Group_1__0__Impl rule__Equiv__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__Equiv__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equiv__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__0"


    // $ANTLR start "rule__Equiv__Group_1__0__Impl"
    // InternalFormula.g:933:1: rule__Equiv__Group_1__0__Impl : ( () ) ;
    public final void rule__Equiv__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:937:1: ( ( () ) )
            // InternalFormula.g:938:1: ( () )
            {
            // InternalFormula.g:938:1: ( () )
            // InternalFormula.g:939:2: ()
            {
             before(grammarAccess.getEquivAccess().getEquivLeftAction_1_0()); 
            // InternalFormula.g:940:2: ()
            // InternalFormula.g:940:3: 
            {
            }

             after(grammarAccess.getEquivAccess().getEquivLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__0__Impl"


    // $ANTLR start "rule__Equiv__Group_1__1"
    // InternalFormula.g:948:1: rule__Equiv__Group_1__1 : rule__Equiv__Group_1__1__Impl rule__Equiv__Group_1__2 ;
    public final void rule__Equiv__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:952:1: ( rule__Equiv__Group_1__1__Impl rule__Equiv__Group_1__2 )
            // InternalFormula.g:953:2: rule__Equiv__Group_1__1__Impl rule__Equiv__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Equiv__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equiv__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__1"


    // $ANTLR start "rule__Equiv__Group_1__1__Impl"
    // InternalFormula.g:960:1: rule__Equiv__Group_1__1__Impl : ( '<=>' ) ;
    public final void rule__Equiv__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:964:1: ( ( '<=>' ) )
            // InternalFormula.g:965:1: ( '<=>' )
            {
            // InternalFormula.g:965:1: ( '<=>' )
            // InternalFormula.g:966:2: '<=>'
            {
             before(grammarAccess.getEquivAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getEquivAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__1__Impl"


    // $ANTLR start "rule__Equiv__Group_1__2"
    // InternalFormula.g:975:1: rule__Equiv__Group_1__2 : rule__Equiv__Group_1__2__Impl ;
    public final void rule__Equiv__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:979:1: ( rule__Equiv__Group_1__2__Impl )
            // InternalFormula.g:980:2: rule__Equiv__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equiv__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__2"


    // $ANTLR start "rule__Equiv__Group_1__2__Impl"
    // InternalFormula.g:986:1: rule__Equiv__Group_1__2__Impl : ( ( rule__Equiv__RightAssignment_1_2 ) ) ;
    public final void rule__Equiv__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:990:1: ( ( ( rule__Equiv__RightAssignment_1_2 ) ) )
            // InternalFormula.g:991:1: ( ( rule__Equiv__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:991:1: ( ( rule__Equiv__RightAssignment_1_2 ) )
            // InternalFormula.g:992:2: ( rule__Equiv__RightAssignment_1_2 )
            {
             before(grammarAccess.getEquivAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:993:2: ( rule__Equiv__RightAssignment_1_2 )
            // InternalFormula.g:993:3: rule__Equiv__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Equiv__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getEquivAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__Group_1__2__Impl"


    // $ANTLR start "rule__Implie__Group__0"
    // InternalFormula.g:1002:1: rule__Implie__Group__0 : rule__Implie__Group__0__Impl rule__Implie__Group__1 ;
    public final void rule__Implie__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1006:1: ( rule__Implie__Group__0__Impl rule__Implie__Group__1 )
            // InternalFormula.g:1007:2: rule__Implie__Group__0__Impl rule__Implie__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Implie__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implie__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group__0"


    // $ANTLR start "rule__Implie__Group__0__Impl"
    // InternalFormula.g:1014:1: rule__Implie__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Implie__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1018:1: ( ( ruleOr ) )
            // InternalFormula.g:1019:1: ( ruleOr )
            {
            // InternalFormula.g:1019:1: ( ruleOr )
            // InternalFormula.g:1020:2: ruleOr
            {
             before(grammarAccess.getImplieAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getImplieAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group__0__Impl"


    // $ANTLR start "rule__Implie__Group__1"
    // InternalFormula.g:1029:1: rule__Implie__Group__1 : rule__Implie__Group__1__Impl ;
    public final void rule__Implie__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1033:1: ( rule__Implie__Group__1__Impl )
            // InternalFormula.g:1034:2: rule__Implie__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implie__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group__1"


    // $ANTLR start "rule__Implie__Group__1__Impl"
    // InternalFormula.g:1040:1: rule__Implie__Group__1__Impl : ( ( rule__Implie__Group_1__0 )* ) ;
    public final void rule__Implie__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1044:1: ( ( ( rule__Implie__Group_1__0 )* ) )
            // InternalFormula.g:1045:1: ( ( rule__Implie__Group_1__0 )* )
            {
            // InternalFormula.g:1045:1: ( ( rule__Implie__Group_1__0 )* )
            // InternalFormula.g:1046:2: ( rule__Implie__Group_1__0 )*
            {
             before(grammarAccess.getImplieAccess().getGroup_1()); 
            // InternalFormula.g:1047:2: ( rule__Implie__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==18) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalFormula.g:1047:3: rule__Implie__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Implie__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getImplieAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group__1__Impl"


    // $ANTLR start "rule__Implie__Group_1__0"
    // InternalFormula.g:1056:1: rule__Implie__Group_1__0 : rule__Implie__Group_1__0__Impl rule__Implie__Group_1__1 ;
    public final void rule__Implie__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1060:1: ( rule__Implie__Group_1__0__Impl rule__Implie__Group_1__1 )
            // InternalFormula.g:1061:2: rule__Implie__Group_1__0__Impl rule__Implie__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Implie__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implie__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__0"


    // $ANTLR start "rule__Implie__Group_1__0__Impl"
    // InternalFormula.g:1068:1: rule__Implie__Group_1__0__Impl : ( () ) ;
    public final void rule__Implie__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1072:1: ( ( () ) )
            // InternalFormula.g:1073:1: ( () )
            {
            // InternalFormula.g:1073:1: ( () )
            // InternalFormula.g:1074:2: ()
            {
             before(grammarAccess.getImplieAccess().getImplieLeftAction_1_0()); 
            // InternalFormula.g:1075:2: ()
            // InternalFormula.g:1075:3: 
            {
            }

             after(grammarAccess.getImplieAccess().getImplieLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__0__Impl"


    // $ANTLR start "rule__Implie__Group_1__1"
    // InternalFormula.g:1083:1: rule__Implie__Group_1__1 : rule__Implie__Group_1__1__Impl rule__Implie__Group_1__2 ;
    public final void rule__Implie__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1087:1: ( rule__Implie__Group_1__1__Impl rule__Implie__Group_1__2 )
            // InternalFormula.g:1088:2: rule__Implie__Group_1__1__Impl rule__Implie__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Implie__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implie__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__1"


    // $ANTLR start "rule__Implie__Group_1__1__Impl"
    // InternalFormula.g:1095:1: rule__Implie__Group_1__1__Impl : ( '=>' ) ;
    public final void rule__Implie__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1099:1: ( ( '=>' ) )
            // InternalFormula.g:1100:1: ( '=>' )
            {
            // InternalFormula.g:1100:1: ( '=>' )
            // InternalFormula.g:1101:2: '=>'
            {
             before(grammarAccess.getImplieAccess().getEqualsSignGreaterThanSignKeyword_1_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getImplieAccess().getEqualsSignGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__1__Impl"


    // $ANTLR start "rule__Implie__Group_1__2"
    // InternalFormula.g:1110:1: rule__Implie__Group_1__2 : rule__Implie__Group_1__2__Impl ;
    public final void rule__Implie__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1114:1: ( rule__Implie__Group_1__2__Impl )
            // InternalFormula.g:1115:2: rule__Implie__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implie__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__2"


    // $ANTLR start "rule__Implie__Group_1__2__Impl"
    // InternalFormula.g:1121:1: rule__Implie__Group_1__2__Impl : ( ( rule__Implie__RightAssignment_1_2 ) ) ;
    public final void rule__Implie__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1125:1: ( ( ( rule__Implie__RightAssignment_1_2 ) ) )
            // InternalFormula.g:1126:1: ( ( rule__Implie__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:1126:1: ( ( rule__Implie__RightAssignment_1_2 ) )
            // InternalFormula.g:1127:2: ( rule__Implie__RightAssignment_1_2 )
            {
             before(grammarAccess.getImplieAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:1128:2: ( rule__Implie__RightAssignment_1_2 )
            // InternalFormula.g:1128:3: rule__Implie__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implie__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImplieAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalFormula.g:1137:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1141:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalFormula.g:1142:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalFormula.g:1149:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1153:1: ( ( ruleAnd ) )
            // InternalFormula.g:1154:1: ( ruleAnd )
            {
            // InternalFormula.g:1154:1: ( ruleAnd )
            // InternalFormula.g:1155:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalFormula.g:1164:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1168:1: ( rule__Or__Group__1__Impl )
            // InternalFormula.g:1169:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalFormula.g:1175:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1179:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalFormula.g:1180:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalFormula.g:1180:1: ( ( rule__Or__Group_1__0 )* )
            // InternalFormula.g:1181:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalFormula.g:1182:2: ( rule__Or__Group_1__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==19) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalFormula.g:1182:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalFormula.g:1191:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1195:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalFormula.g:1196:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalFormula.g:1203:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1207:1: ( ( () ) )
            // InternalFormula.g:1208:1: ( () )
            {
            // InternalFormula.g:1208:1: ( () )
            // InternalFormula.g:1209:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalFormula.g:1210:2: ()
            // InternalFormula.g:1210:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalFormula.g:1218:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1222:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalFormula.g:1223:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalFormula.g:1230:1: rule__Or__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1234:1: ( ( 'or' ) )
            // InternalFormula.g:1235:1: ( 'or' )
            {
            // InternalFormula.g:1235:1: ( 'or' )
            // InternalFormula.g:1236:2: 'or'
            {
             before(grammarAccess.getOrAccess().getOrKeyword_1_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalFormula.g:1245:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1249:1: ( rule__Or__Group_1__2__Impl )
            // InternalFormula.g:1250:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalFormula.g:1256:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1260:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalFormula.g:1261:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:1261:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalFormula.g:1262:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:1263:2: ( rule__Or__RightAssignment_1_2 )
            // InternalFormula.g:1263:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__Nor__Group__0"
    // InternalFormula.g:1272:1: rule__Nor__Group__0 : rule__Nor__Group__0__Impl rule__Nor__Group__1 ;
    public final void rule__Nor__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1276:1: ( rule__Nor__Group__0__Impl rule__Nor__Group__1 )
            // InternalFormula.g:1277:2: rule__Nor__Group__0__Impl rule__Nor__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Nor__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nor__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group__0"


    // $ANTLR start "rule__Nor__Group__0__Impl"
    // InternalFormula.g:1284:1: rule__Nor__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Nor__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1288:1: ( ( ruleAnd ) )
            // InternalFormula.g:1289:1: ( ruleAnd )
            {
            // InternalFormula.g:1289:1: ( ruleAnd )
            // InternalFormula.g:1290:2: ruleAnd
            {
             before(grammarAccess.getNorAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getNorAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group__0__Impl"


    // $ANTLR start "rule__Nor__Group__1"
    // InternalFormula.g:1299:1: rule__Nor__Group__1 : rule__Nor__Group__1__Impl ;
    public final void rule__Nor__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1303:1: ( rule__Nor__Group__1__Impl )
            // InternalFormula.g:1304:2: rule__Nor__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Nor__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group__1"


    // $ANTLR start "rule__Nor__Group__1__Impl"
    // InternalFormula.g:1310:1: rule__Nor__Group__1__Impl : ( ( rule__Nor__Group_1__0 )* ) ;
    public final void rule__Nor__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1314:1: ( ( ( rule__Nor__Group_1__0 )* ) )
            // InternalFormula.g:1315:1: ( ( rule__Nor__Group_1__0 )* )
            {
            // InternalFormula.g:1315:1: ( ( rule__Nor__Group_1__0 )* )
            // InternalFormula.g:1316:2: ( rule__Nor__Group_1__0 )*
            {
             before(grammarAccess.getNorAccess().getGroup_1()); 
            // InternalFormula.g:1317:2: ( rule__Nor__Group_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==20) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalFormula.g:1317:3: rule__Nor__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__Nor__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getNorAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group__1__Impl"


    // $ANTLR start "rule__Nor__Group_1__0"
    // InternalFormula.g:1326:1: rule__Nor__Group_1__0 : rule__Nor__Group_1__0__Impl rule__Nor__Group_1__1 ;
    public final void rule__Nor__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1330:1: ( rule__Nor__Group_1__0__Impl rule__Nor__Group_1__1 )
            // InternalFormula.g:1331:2: rule__Nor__Group_1__0__Impl rule__Nor__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Nor__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nor__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__0"


    // $ANTLR start "rule__Nor__Group_1__0__Impl"
    // InternalFormula.g:1338:1: rule__Nor__Group_1__0__Impl : ( () ) ;
    public final void rule__Nor__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1342:1: ( ( () ) )
            // InternalFormula.g:1343:1: ( () )
            {
            // InternalFormula.g:1343:1: ( () )
            // InternalFormula.g:1344:2: ()
            {
             before(grammarAccess.getNorAccess().getNorLeftAction_1_0()); 
            // InternalFormula.g:1345:2: ()
            // InternalFormula.g:1345:3: 
            {
            }

             after(grammarAccess.getNorAccess().getNorLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__0__Impl"


    // $ANTLR start "rule__Nor__Group_1__1"
    // InternalFormula.g:1353:1: rule__Nor__Group_1__1 : rule__Nor__Group_1__1__Impl rule__Nor__Group_1__2 ;
    public final void rule__Nor__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1357:1: ( rule__Nor__Group_1__1__Impl rule__Nor__Group_1__2 )
            // InternalFormula.g:1358:2: rule__Nor__Group_1__1__Impl rule__Nor__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Nor__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nor__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__1"


    // $ANTLR start "rule__Nor__Group_1__1__Impl"
    // InternalFormula.g:1365:1: rule__Nor__Group_1__1__Impl : ( 'nor' ) ;
    public final void rule__Nor__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1369:1: ( ( 'nor' ) )
            // InternalFormula.g:1370:1: ( 'nor' )
            {
            // InternalFormula.g:1370:1: ( 'nor' )
            // InternalFormula.g:1371:2: 'nor'
            {
             before(grammarAccess.getNorAccess().getNorKeyword_1_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getNorAccess().getNorKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__1__Impl"


    // $ANTLR start "rule__Nor__Group_1__2"
    // InternalFormula.g:1380:1: rule__Nor__Group_1__2 : rule__Nor__Group_1__2__Impl ;
    public final void rule__Nor__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1384:1: ( rule__Nor__Group_1__2__Impl )
            // InternalFormula.g:1385:2: rule__Nor__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Nor__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__2"


    // $ANTLR start "rule__Nor__Group_1__2__Impl"
    // InternalFormula.g:1391:1: rule__Nor__Group_1__2__Impl : ( ( rule__Nor__RightAssignment_1_2 ) ) ;
    public final void rule__Nor__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1395:1: ( ( ( rule__Nor__RightAssignment_1_2 ) ) )
            // InternalFormula.g:1396:1: ( ( rule__Nor__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:1396:1: ( ( rule__Nor__RightAssignment_1_2 ) )
            // InternalFormula.g:1397:2: ( rule__Nor__RightAssignment_1_2 )
            {
             before(grammarAccess.getNorAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:1398:2: ( rule__Nor__RightAssignment_1_2 )
            // InternalFormula.g:1398:3: rule__Nor__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Nor__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getNorAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalFormula.g:1407:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1411:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalFormula.g:1412:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalFormula.g:1419:1: rule__And__Group__0__Impl : ( ruleNand ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1423:1: ( ( ruleNand ) )
            // InternalFormula.g:1424:1: ( ruleNand )
            {
            // InternalFormula.g:1424:1: ( ruleNand )
            // InternalFormula.g:1425:2: ruleNand
            {
             before(grammarAccess.getAndAccess().getNandParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNand();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNandParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalFormula.g:1434:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1438:1: ( rule__And__Group__1__Impl )
            // InternalFormula.g:1439:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalFormula.g:1445:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1449:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalFormula.g:1450:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalFormula.g:1450:1: ( ( rule__And__Group_1__0 )* )
            // InternalFormula.g:1451:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalFormula.g:1452:2: ( rule__And__Group_1__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==21) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalFormula.g:1452:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalFormula.g:1461:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1465:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalFormula.g:1466:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalFormula.g:1473:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1477:1: ( ( () ) )
            // InternalFormula.g:1478:1: ( () )
            {
            // InternalFormula.g:1478:1: ( () )
            // InternalFormula.g:1479:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalFormula.g:1480:2: ()
            // InternalFormula.g:1480:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalFormula.g:1488:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1492:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalFormula.g:1493:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalFormula.g:1500:1: rule__And__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1504:1: ( ( 'and' ) )
            // InternalFormula.g:1505:1: ( 'and' )
            {
            // InternalFormula.g:1505:1: ( 'and' )
            // InternalFormula.g:1506:2: 'and'
            {
             before(grammarAccess.getAndAccess().getAndKeyword_1_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalFormula.g:1515:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1519:1: ( rule__And__Group_1__2__Impl )
            // InternalFormula.g:1520:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalFormula.g:1526:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1530:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalFormula.g:1531:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:1531:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalFormula.g:1532:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:1533:2: ( rule__And__RightAssignment_1_2 )
            // InternalFormula.g:1533:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Nand__Group__0"
    // InternalFormula.g:1542:1: rule__Nand__Group__0 : rule__Nand__Group__0__Impl rule__Nand__Group__1 ;
    public final void rule__Nand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1546:1: ( rule__Nand__Group__0__Impl rule__Nand__Group__1 )
            // InternalFormula.g:1547:2: rule__Nand__Group__0__Impl rule__Nand__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Nand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group__0"


    // $ANTLR start "rule__Nand__Group__0__Impl"
    // InternalFormula.g:1554:1: rule__Nand__Group__0__Impl : ( ( rule__Nand__Alternatives_0 ) ) ;
    public final void rule__Nand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1558:1: ( ( ( rule__Nand__Alternatives_0 ) ) )
            // InternalFormula.g:1559:1: ( ( rule__Nand__Alternatives_0 ) )
            {
            // InternalFormula.g:1559:1: ( ( rule__Nand__Alternatives_0 ) )
            // InternalFormula.g:1560:2: ( rule__Nand__Alternatives_0 )
            {
             before(grammarAccess.getNandAccess().getAlternatives_0()); 
            // InternalFormula.g:1561:2: ( rule__Nand__Alternatives_0 )
            // InternalFormula.g:1561:3: rule__Nand__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Nand__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getNandAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group__0__Impl"


    // $ANTLR start "rule__Nand__Group__1"
    // InternalFormula.g:1569:1: rule__Nand__Group__1 : rule__Nand__Group__1__Impl ;
    public final void rule__Nand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1573:1: ( rule__Nand__Group__1__Impl )
            // InternalFormula.g:1574:2: rule__Nand__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Nand__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group__1"


    // $ANTLR start "rule__Nand__Group__1__Impl"
    // InternalFormula.g:1580:1: rule__Nand__Group__1__Impl : ( ( rule__Nand__Group_1__0 )* ) ;
    public final void rule__Nand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1584:1: ( ( ( rule__Nand__Group_1__0 )* ) )
            // InternalFormula.g:1585:1: ( ( rule__Nand__Group_1__0 )* )
            {
            // InternalFormula.g:1585:1: ( ( rule__Nand__Group_1__0 )* )
            // InternalFormula.g:1586:2: ( rule__Nand__Group_1__0 )*
            {
             before(grammarAccess.getNandAccess().getGroup_1()); 
            // InternalFormula.g:1587:2: ( rule__Nand__Group_1__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==22) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalFormula.g:1587:3: rule__Nand__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__Nand__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getNandAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group__1__Impl"


    // $ANTLR start "rule__Nand__Group_1__0"
    // InternalFormula.g:1596:1: rule__Nand__Group_1__0 : rule__Nand__Group_1__0__Impl rule__Nand__Group_1__1 ;
    public final void rule__Nand__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1600:1: ( rule__Nand__Group_1__0__Impl rule__Nand__Group_1__1 )
            // InternalFormula.g:1601:2: rule__Nand__Group_1__0__Impl rule__Nand__Group_1__1
            {
            pushFollow(FOLLOW_19);
            rule__Nand__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nand__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__0"


    // $ANTLR start "rule__Nand__Group_1__0__Impl"
    // InternalFormula.g:1608:1: rule__Nand__Group_1__0__Impl : ( () ) ;
    public final void rule__Nand__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1612:1: ( ( () ) )
            // InternalFormula.g:1613:1: ( () )
            {
            // InternalFormula.g:1613:1: ( () )
            // InternalFormula.g:1614:2: ()
            {
             before(grammarAccess.getNandAccess().getNandLeftAction_1_0()); 
            // InternalFormula.g:1615:2: ()
            // InternalFormula.g:1615:3: 
            {
            }

             after(grammarAccess.getNandAccess().getNandLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__0__Impl"


    // $ANTLR start "rule__Nand__Group_1__1"
    // InternalFormula.g:1623:1: rule__Nand__Group_1__1 : rule__Nand__Group_1__1__Impl rule__Nand__Group_1__2 ;
    public final void rule__Nand__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1627:1: ( rule__Nand__Group_1__1__Impl rule__Nand__Group_1__2 )
            // InternalFormula.g:1628:2: rule__Nand__Group_1__1__Impl rule__Nand__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Nand__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Nand__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__1"


    // $ANTLR start "rule__Nand__Group_1__1__Impl"
    // InternalFormula.g:1635:1: rule__Nand__Group_1__1__Impl : ( 'nand' ) ;
    public final void rule__Nand__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1639:1: ( ( 'nand' ) )
            // InternalFormula.g:1640:1: ( 'nand' )
            {
            // InternalFormula.g:1640:1: ( 'nand' )
            // InternalFormula.g:1641:2: 'nand'
            {
             before(grammarAccess.getNandAccess().getNandKeyword_1_1()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getNandAccess().getNandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__1__Impl"


    // $ANTLR start "rule__Nand__Group_1__2"
    // InternalFormula.g:1650:1: rule__Nand__Group_1__2 : rule__Nand__Group_1__2__Impl ;
    public final void rule__Nand__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1654:1: ( rule__Nand__Group_1__2__Impl )
            // InternalFormula.g:1655:2: rule__Nand__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Nand__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__2"


    // $ANTLR start "rule__Nand__Group_1__2__Impl"
    // InternalFormula.g:1661:1: rule__Nand__Group_1__2__Impl : ( ( rule__Nand__RightAssignment_1_2 ) ) ;
    public final void rule__Nand__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1665:1: ( ( ( rule__Nand__RightAssignment_1_2 ) ) )
            // InternalFormula.g:1666:1: ( ( rule__Nand__RightAssignment_1_2 ) )
            {
            // InternalFormula.g:1666:1: ( ( rule__Nand__RightAssignment_1_2 ) )
            // InternalFormula.g:1667:2: ( rule__Nand__RightAssignment_1_2 )
            {
             before(grammarAccess.getNandAccess().getRightAssignment_1_2()); 
            // InternalFormula.g:1668:2: ( rule__Nand__RightAssignment_1_2 )
            // InternalFormula.g:1668:3: rule__Nand__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Nand__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getNandAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group__0"
    // InternalFormula.g:1677:1: rule__Not__Group__0 : rule__Not__Group__0__Impl rule__Not__Group__1 ;
    public final void rule__Not__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1681:1: ( rule__Not__Group__0__Impl rule__Not__Group__1 )
            // InternalFormula.g:1682:2: rule__Not__Group__0__Impl rule__Not__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Not__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0"


    // $ANTLR start "rule__Not__Group__0__Impl"
    // InternalFormula.g:1689:1: rule__Not__Group__0__Impl : ( '!' ) ;
    public final void rule__Not__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1693:1: ( ( '!' ) )
            // InternalFormula.g:1694:1: ( '!' )
            {
            // InternalFormula.g:1694:1: ( '!' )
            // InternalFormula.g:1695:2: '!'
            {
             before(grammarAccess.getNotAccess().getExclamationMarkKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getExclamationMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0__Impl"


    // $ANTLR start "rule__Not__Group__1"
    // InternalFormula.g:1704:1: rule__Not__Group__1 : rule__Not__Group__1__Impl rule__Not__Group__2 ;
    public final void rule__Not__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1708:1: ( rule__Not__Group__1__Impl rule__Not__Group__2 )
            // InternalFormula.g:1709:2: rule__Not__Group__1__Impl rule__Not__Group__2
            {
            pushFollow(FOLLOW_1);
            rule__Not__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1"


    // $ANTLR start "rule__Not__Group__1__Impl"
    // InternalFormula.g:1716:1: rule__Not__Group__1__Impl : ( rulePrimary ) ;
    public final void rule__Not__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1720:1: ( ( rulePrimary ) )
            // InternalFormula.g:1721:1: ( rulePrimary )
            {
            // InternalFormula.g:1721:1: ( rulePrimary )
            // InternalFormula.g:1722:2: rulePrimary
            {
             before(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1__Impl"


    // $ANTLR start "rule__Not__Group__2"
    // InternalFormula.g:1731:1: rule__Not__Group__2 : rule__Not__Group__2__Impl ;
    public final void rule__Not__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1735:1: ( rule__Not__Group__2__Impl )
            // InternalFormula.g:1736:2: rule__Not__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__2"


    // $ANTLR start "rule__Not__Group__2__Impl"
    // InternalFormula.g:1742:1: rule__Not__Group__2__Impl : ( () ) ;
    public final void rule__Not__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1746:1: ( ( () ) )
            // InternalFormula.g:1747:1: ( () )
            {
            // InternalFormula.g:1747:1: ( () )
            // InternalFormula.g:1748:2: ()
            {
             before(grammarAccess.getNotAccess().getNotExpAction_2()); 
            // InternalFormula.g:1749:2: ()
            // InternalFormula.g:1749:3: 
            {
            }

             after(grammarAccess.getNotAccess().getNotExpAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__2__Impl"


    // $ANTLR start "rule__Atomic__Group_0__0"
    // InternalFormula.g:1758:1: rule__Atomic__Group_0__0 : rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 ;
    public final void rule__Atomic__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1762:1: ( rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 )
            // InternalFormula.g:1763:2: rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1
            {
            pushFollow(FOLLOW_21);
            rule__Atomic__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0"


    // $ANTLR start "rule__Atomic__Group_0__0__Impl"
    // InternalFormula.g:1770:1: rule__Atomic__Group_0__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1774:1: ( ( () ) )
            // InternalFormula.g:1775:1: ( () )
            {
            // InternalFormula.g:1775:1: ( () )
            // InternalFormula.g:1776:2: ()
            {
             before(grammarAccess.getAtomicAccess().getVarPropAction_0_0()); 
            // InternalFormula.g:1777:2: ()
            // InternalFormula.g:1777:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getVarPropAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0__Impl"


    // $ANTLR start "rule__Atomic__Group_0__1"
    // InternalFormula.g:1785:1: rule__Atomic__Group_0__1 : rule__Atomic__Group_0__1__Impl ;
    public final void rule__Atomic__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1789:1: ( rule__Atomic__Group_0__1__Impl )
            // InternalFormula.g:1790:2: rule__Atomic__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1"


    // $ANTLR start "rule__Atomic__Group_0__1__Impl"
    // InternalFormula.g:1796:1: rule__Atomic__Group_0__1__Impl : ( ( rule__Atomic__IdAssignment_0_1 ) ) ;
    public final void rule__Atomic__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1800:1: ( ( ( rule__Atomic__IdAssignment_0_1 ) ) )
            // InternalFormula.g:1801:1: ( ( rule__Atomic__IdAssignment_0_1 ) )
            {
            // InternalFormula.g:1801:1: ( ( rule__Atomic__IdAssignment_0_1 ) )
            // InternalFormula.g:1802:2: ( rule__Atomic__IdAssignment_0_1 )
            {
             before(grammarAccess.getAtomicAccess().getIdAssignment_0_1()); 
            // InternalFormula.g:1803:2: ( rule__Atomic__IdAssignment_0_1 )
            // InternalFormula.g:1803:3: rule__Atomic__IdAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__IdAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getIdAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1__Impl"


    // $ANTLR start "rule__Atomic__Group_1__0"
    // InternalFormula.g:1812:1: rule__Atomic__Group_1__0 : rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 ;
    public final void rule__Atomic__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1816:1: ( rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 )
            // InternalFormula.g:1817:2: rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1
            {
            pushFollow(FOLLOW_22);
            rule__Atomic__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0"


    // $ANTLR start "rule__Atomic__Group_1__0__Impl"
    // InternalFormula.g:1824:1: rule__Atomic__Group_1__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1828:1: ( ( () ) )
            // InternalFormula.g:1829:1: ( () )
            {
            // InternalFormula.g:1829:1: ( () )
            // InternalFormula.g:1830:2: ()
            {
             before(grammarAccess.getAtomicAccess().getBoolConstantAction_1_0()); 
            // InternalFormula.g:1831:2: ()
            // InternalFormula.g:1831:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getBoolConstantAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0__Impl"


    // $ANTLR start "rule__Atomic__Group_1__1"
    // InternalFormula.g:1839:1: rule__Atomic__Group_1__1 : rule__Atomic__Group_1__1__Impl ;
    public final void rule__Atomic__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1843:1: ( rule__Atomic__Group_1__1__Impl )
            // InternalFormula.g:1844:2: rule__Atomic__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1"


    // $ANTLR start "rule__Atomic__Group_1__1__Impl"
    // InternalFormula.g:1850:1: rule__Atomic__Group_1__1__Impl : ( ( rule__Atomic__ValueAssignment_1_1 ) ) ;
    public final void rule__Atomic__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1854:1: ( ( ( rule__Atomic__ValueAssignment_1_1 ) ) )
            // InternalFormula.g:1855:1: ( ( rule__Atomic__ValueAssignment_1_1 ) )
            {
            // InternalFormula.g:1855:1: ( ( rule__Atomic__ValueAssignment_1_1 ) )
            // InternalFormula.g:1856:2: ( rule__Atomic__ValueAssignment_1_1 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_1_1()); 
            // InternalFormula.g:1857:2: ( rule__Atomic__ValueAssignment_1_1 )
            // InternalFormula.g:1857:3: rule__Atomic__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1__Impl"


    // $ANTLR start "rule__PFormula__Group__0"
    // InternalFormula.g:1866:1: rule__PFormula__Group__0 : rule__PFormula__Group__0__Impl rule__PFormula__Group__1 ;
    public final void rule__PFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1870:1: ( rule__PFormula__Group__0__Impl rule__PFormula__Group__1 )
            // InternalFormula.g:1871:2: rule__PFormula__Group__0__Impl rule__PFormula__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__PFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__0"


    // $ANTLR start "rule__PFormula__Group__0__Impl"
    // InternalFormula.g:1878:1: rule__PFormula__Group__0__Impl : ( '(' ) ;
    public final void rule__PFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1882:1: ( ( '(' ) )
            // InternalFormula.g:1883:1: ( '(' )
            {
            // InternalFormula.g:1883:1: ( '(' )
            // InternalFormula.g:1884:2: '('
            {
             before(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__0__Impl"


    // $ANTLR start "rule__PFormula__Group__1"
    // InternalFormula.g:1893:1: rule__PFormula__Group__1 : rule__PFormula__Group__1__Impl rule__PFormula__Group__2 ;
    public final void rule__PFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1897:1: ( rule__PFormula__Group__1__Impl rule__PFormula__Group__2 )
            // InternalFormula.g:1898:2: rule__PFormula__Group__1__Impl rule__PFormula__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__PFormula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__1"


    // $ANTLR start "rule__PFormula__Group__1__Impl"
    // InternalFormula.g:1905:1: rule__PFormula__Group__1__Impl : ( ruleEquiv ) ;
    public final void rule__PFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1909:1: ( ( ruleEquiv ) )
            // InternalFormula.g:1910:1: ( ruleEquiv )
            {
            // InternalFormula.g:1910:1: ( ruleEquiv )
            // InternalFormula.g:1911:2: ruleEquiv
            {
             before(grammarAccess.getPFormulaAccess().getEquivParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleEquiv();

            state._fsp--;

             after(grammarAccess.getPFormulaAccess().getEquivParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__1__Impl"


    // $ANTLR start "rule__PFormula__Group__2"
    // InternalFormula.g:1920:1: rule__PFormula__Group__2 : rule__PFormula__Group__2__Impl rule__PFormula__Group__3 ;
    public final void rule__PFormula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1924:1: ( rule__PFormula__Group__2__Impl rule__PFormula__Group__3 )
            // InternalFormula.g:1925:2: rule__PFormula__Group__2__Impl rule__PFormula__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__PFormula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__2"


    // $ANTLR start "rule__PFormula__Group__2__Impl"
    // InternalFormula.g:1932:1: rule__PFormula__Group__2__Impl : ( () ) ;
    public final void rule__PFormula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1936:1: ( ( () ) )
            // InternalFormula.g:1937:1: ( () )
            {
            // InternalFormula.g:1937:1: ( () )
            // InternalFormula.g:1938:2: ()
            {
             before(grammarAccess.getPFormulaAccess().getPFormulaExpAction_2()); 
            // InternalFormula.g:1939:2: ()
            // InternalFormula.g:1939:3: 
            {
            }

             after(grammarAccess.getPFormulaAccess().getPFormulaExpAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__2__Impl"


    // $ANTLR start "rule__PFormula__Group__3"
    // InternalFormula.g:1947:1: rule__PFormula__Group__3 : rule__PFormula__Group__3__Impl ;
    public final void rule__PFormula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1951:1: ( rule__PFormula__Group__3__Impl )
            // InternalFormula.g:1952:2: rule__PFormula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PFormula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__3"


    // $ANTLR start "rule__PFormula__Group__3__Impl"
    // InternalFormula.g:1958:1: rule__PFormula__Group__3__Impl : ( ')' ) ;
    public final void rule__PFormula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1962:1: ( ( ')' ) )
            // InternalFormula.g:1963:1: ( ')' )
            {
            // InternalFormula.g:1963:1: ( ')' )
            // InternalFormula.g:1964:2: ')'
            {
             before(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__3__Impl"


    // $ANTLR start "rule__Solve__TestsAssignment_2"
    // InternalFormula.g:1974:1: rule__Solve__TestsAssignment_2 : ( ( rule__Solve__TestsAlternatives_2_0 ) ) ;
    public final void rule__Solve__TestsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1978:1: ( ( ( rule__Solve__TestsAlternatives_2_0 ) ) )
            // InternalFormula.g:1979:2: ( ( rule__Solve__TestsAlternatives_2_0 ) )
            {
            // InternalFormula.g:1979:2: ( ( rule__Solve__TestsAlternatives_2_0 ) )
            // InternalFormula.g:1980:3: ( rule__Solve__TestsAlternatives_2_0 )
            {
             before(grammarAccess.getSolveAccess().getTestsAlternatives_2_0()); 
            // InternalFormula.g:1981:3: ( rule__Solve__TestsAlternatives_2_0 )
            // InternalFormula.g:1981:4: rule__Solve__TestsAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Solve__TestsAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getSolveAccess().getTestsAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__TestsAssignment_2"


    // $ANTLR start "rule__Test__FormulaAssignment_1"
    // InternalFormula.g:1989:1: rule__Test__FormulaAssignment_1 : ( ruleEquiv ) ;
    public final void rule__Test__FormulaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:1993:1: ( ( ruleEquiv ) )
            // InternalFormula.g:1994:2: ( ruleEquiv )
            {
            // InternalFormula.g:1994:2: ( ruleEquiv )
            // InternalFormula.g:1995:3: ruleEquiv
            {
             before(grammarAccess.getTestAccess().getFormulaEquivParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEquiv();

            state._fsp--;

             after(grammarAccess.getTestAccess().getFormulaEquivParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__FormulaAssignment_1"


    // $ANTLR start "rule__Test__SolverAssignment_3"
    // InternalFormula.g:2004:1: rule__Test__SolverAssignment_3 : ( ruleSatsolver ) ;
    public final void rule__Test__SolverAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2008:1: ( ( ruleSatsolver ) )
            // InternalFormula.g:2009:2: ( ruleSatsolver )
            {
            // InternalFormula.g:2009:2: ( ruleSatsolver )
            // InternalFormula.g:2010:3: ruleSatsolver
            {
             before(grammarAccess.getTestAccess().getSolverSatsolverParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleSatsolver();

            state._fsp--;

             after(grammarAccess.getTestAccess().getSolverSatsolverParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Test__SolverAssignment_3"


    // $ANTLR start "rule__AllSolver__FormulaAssignment_1"
    // InternalFormula.g:2019:1: rule__AllSolver__FormulaAssignment_1 : ( ruleEquiv ) ;
    public final void rule__AllSolver__FormulaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2023:1: ( ( ruleEquiv ) )
            // InternalFormula.g:2024:2: ( ruleEquiv )
            {
            // InternalFormula.g:2024:2: ( ruleEquiv )
            // InternalFormula.g:2025:3: ruleEquiv
            {
             before(grammarAccess.getAllSolverAccess().getFormulaEquivParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEquiv();

            state._fsp--;

             after(grammarAccess.getAllSolverAccess().getFormulaEquivParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllSolver__FormulaAssignment_1"


    // $ANTLR start "rule__Satsolver__NameAssignment_0"
    // InternalFormula.g:2034:1: rule__Satsolver__NameAssignment_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__Satsolver__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2038:1: ( ( ( 'SAT4J' ) ) )
            // InternalFormula.g:2039:2: ( ( 'SAT4J' ) )
            {
            // InternalFormula.g:2039:2: ( ( 'SAT4J' ) )
            // InternalFormula.g:2040:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSatsolverAccess().getNameSAT4JKeyword_0_0()); 
            // InternalFormula.g:2041:3: ( 'SAT4J' )
            // InternalFormula.g:2042:4: 'SAT4J'
            {
             before(grammarAccess.getSatsolverAccess().getNameSAT4JKeyword_0_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getSatsolverAccess().getNameSAT4JKeyword_0_0()); 

            }

             after(grammarAccess.getSatsolverAccess().getNameSAT4JKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Satsolver__NameAssignment_0"


    // $ANTLR start "rule__Satsolver__NameAssignment_1"
    // InternalFormula.g:2053:1: rule__Satsolver__NameAssignment_1 : ( ( 'minisat' ) ) ;
    public final void rule__Satsolver__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2057:1: ( ( ( 'minisat' ) ) )
            // InternalFormula.g:2058:2: ( ( 'minisat' ) )
            {
            // InternalFormula.g:2058:2: ( ( 'minisat' ) )
            // InternalFormula.g:2059:3: ( 'minisat' )
            {
             before(grammarAccess.getSatsolverAccess().getNameMinisatKeyword_1_0()); 
            // InternalFormula.g:2060:3: ( 'minisat' )
            // InternalFormula.g:2061:4: 'minisat'
            {
             before(grammarAccess.getSatsolverAccess().getNameMinisatKeyword_1_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getSatsolverAccess().getNameMinisatKeyword_1_0()); 

            }

             after(grammarAccess.getSatsolverAccess().getNameMinisatKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Satsolver__NameAssignment_1"


    // $ANTLR start "rule__Equiv__RightAssignment_1_2"
    // InternalFormula.g:2072:1: rule__Equiv__RightAssignment_1_2 : ( ruleImplie ) ;
    public final void rule__Equiv__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2076:1: ( ( ruleImplie ) )
            // InternalFormula.g:2077:2: ( ruleImplie )
            {
            // InternalFormula.g:2077:2: ( ruleImplie )
            // InternalFormula.g:2078:3: ruleImplie
            {
             before(grammarAccess.getEquivAccess().getRightImplieParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplie();

            state._fsp--;

             after(grammarAccess.getEquivAccess().getRightImplieParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equiv__RightAssignment_1_2"


    // $ANTLR start "rule__Implie__RightAssignment_1_2"
    // InternalFormula.g:2087:1: rule__Implie__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Implie__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2091:1: ( ( ruleOr ) )
            // InternalFormula.g:2092:2: ( ruleOr )
            {
            // InternalFormula.g:2092:2: ( ruleOr )
            // InternalFormula.g:2093:3: ruleOr
            {
             before(grammarAccess.getImplieAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getImplieAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implie__RightAssignment_1_2"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalFormula.g:2102:1: rule__Or__RightAssignment_1_2 : ( ruleNor ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2106:1: ( ( ruleNor ) )
            // InternalFormula.g:2107:2: ( ruleNor )
            {
            // InternalFormula.g:2107:2: ( ruleNor )
            // InternalFormula.g:2108:3: ruleNor
            {
             before(grammarAccess.getOrAccess().getRightNorParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNor();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightNorParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__Nor__RightAssignment_1_2"
    // InternalFormula.g:2117:1: rule__Nor__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Nor__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2121:1: ( ( ruleAnd ) )
            // InternalFormula.g:2122:2: ( ruleAnd )
            {
            // InternalFormula.g:2122:2: ( ruleAnd )
            // InternalFormula.g:2123:3: ruleAnd
            {
             before(grammarAccess.getNorAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getNorAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nor__RightAssignment_1_2"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalFormula.g:2132:1: rule__And__RightAssignment_1_2 : ( ruleNand ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2136:1: ( ( ruleNand ) )
            // InternalFormula.g:2137:2: ( ruleNand )
            {
            // InternalFormula.g:2137:2: ( ruleNand )
            // InternalFormula.g:2138:3: ruleNand
            {
             before(grammarAccess.getAndAccess().getRightNandParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNand();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNandParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Nand__RightAssignment_1_2"
    // InternalFormula.g:2147:1: rule__Nand__RightAssignment_1_2 : ( ( rule__Nand__RightAlternatives_1_2_0 ) ) ;
    public final void rule__Nand__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2151:1: ( ( ( rule__Nand__RightAlternatives_1_2_0 ) ) )
            // InternalFormula.g:2152:2: ( ( rule__Nand__RightAlternatives_1_2_0 ) )
            {
            // InternalFormula.g:2152:2: ( ( rule__Nand__RightAlternatives_1_2_0 ) )
            // InternalFormula.g:2153:3: ( rule__Nand__RightAlternatives_1_2_0 )
            {
             before(grammarAccess.getNandAccess().getRightAlternatives_1_2_0()); 
            // InternalFormula.g:2154:3: ( rule__Nand__RightAlternatives_1_2_0 )
            // InternalFormula.g:2154:4: rule__Nand__RightAlternatives_1_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Nand__RightAlternatives_1_2_0();

            state._fsp--;


            }

             after(grammarAccess.getNandAccess().getRightAlternatives_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Nand__RightAssignment_1_2"


    // $ANTLR start "rule__Atomic__IdAssignment_0_1"
    // InternalFormula.g:2162:1: rule__Atomic__IdAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__Atomic__IdAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2166:1: ( ( RULE_ID ) )
            // InternalFormula.g:2167:2: ( RULE_ID )
            {
            // InternalFormula.g:2167:2: ( RULE_ID )
            // InternalFormula.g:2168:3: RULE_ID
            {
             before(grammarAccess.getAtomicAccess().getIdIDTerminalRuleCall_0_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getIdIDTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__IdAssignment_0_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_1_1"
    // InternalFormula.g:2177:1: rule__Atomic__ValueAssignment_1_1 : ( ( rule__Atomic__ValueAlternatives_1_1_0 ) ) ;
    public final void rule__Atomic__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:2181:1: ( ( ( rule__Atomic__ValueAlternatives_1_1_0 ) ) )
            // InternalFormula.g:2182:2: ( ( rule__Atomic__ValueAlternatives_1_1_0 ) )
            {
            // InternalFormula.g:2182:2: ( ( rule__Atomic__ValueAlternatives_1_1_0 ) )
            // InternalFormula.g:2183:3: ( rule__Atomic__ValueAlternatives_1_1_0 )
            {
             before(grammarAccess.getAtomicAccess().getValueAlternatives_1_1_0()); 
            // InternalFormula.g:2184:3: ( rule__Atomic__ValueAlternatives_1_1_0 )
            // InternalFormula.g:2184:4: rule__Atomic__ValueAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_1_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000012002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000001801810L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000001810L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000002000000L});

}