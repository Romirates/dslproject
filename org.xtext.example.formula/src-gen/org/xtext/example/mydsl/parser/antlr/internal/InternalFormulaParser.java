package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.FormulaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFormulaParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Test'", "'with'", "';'", "'AllSolver'", "'SAT4J'", "'minisat'", "'<=>'", "'=>'", "'or'", "'nor'", "'and'", "'nand'", "'!'", "'true'", "'false'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFormulaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFormulaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFormulaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFormula.g"; }



     	private FormulaGrammarAccess grammarAccess;

        public InternalFormulaParser(TokenStream input, FormulaGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Solve";
       	}

       	@Override
       	protected FormulaGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSolve"
    // InternalFormula.g:64:1: entryRuleSolve returns [EObject current=null] : iv_ruleSolve= ruleSolve EOF ;
    public final EObject entryRuleSolve() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolve = null;


        try {
            // InternalFormula.g:64:46: (iv_ruleSolve= ruleSolve EOF )
            // InternalFormula.g:65:2: iv_ruleSolve= ruleSolve EOF
            {
             newCompositeNode(grammarAccess.getSolveRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSolve=ruleSolve();

            state._fsp--;

             current =iv_ruleSolve; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolve"


    // $ANTLR start "ruleSolve"
    // InternalFormula.g:71:1: ruleSolve returns [EObject current=null] : ( (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest ) () ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )* ) ;
    public final EObject ruleSolve() throws RecognitionException {
        EObject current = null;

        EObject this_AllSolver_0 = null;

        EObject this_Test_1 = null;

        EObject lv_tests_3_1 = null;

        EObject lv_tests_3_2 = null;



        	enterRule();

        try {
            // InternalFormula.g:77:2: ( ( (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest ) () ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )* ) )
            // InternalFormula.g:78:2: ( (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest ) () ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )* )
            {
            // InternalFormula.g:78:2: ( (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest ) () ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )* )
            // InternalFormula.g:79:3: (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest ) () ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )*
            {
            // InternalFormula.g:79:3: (this_AllSolver_0= ruleAllSolver | this_Test_1= ruleTest )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            else if ( (LA1_0==11) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalFormula.g:80:4: this_AllSolver_0= ruleAllSolver
                    {

                    				newCompositeNode(grammarAccess.getSolveAccess().getAllSolverParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_3);
                    this_AllSolver_0=ruleAllSolver();

                    state._fsp--;


                    				current = this_AllSolver_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalFormula.g:89:4: this_Test_1= ruleTest
                    {

                    				newCompositeNode(grammarAccess.getSolveAccess().getTestParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_3);
                    this_Test_1=ruleTest();

                    state._fsp--;


                    				current = this_Test_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalFormula.g:98:3: ()
            // InternalFormula.g:99:4: 
            {

            				current = forceCreateModelElementAndAdd(
            					grammarAccess.getSolveAccess().getSolveTestsAction_1(),
            					current);
            			

            }

            // InternalFormula.g:105:3: ( ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==11||LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalFormula.g:106:4: ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) )
            	    {
            	    // InternalFormula.g:106:4: ( (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver ) )
            	    // InternalFormula.g:107:5: (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver )
            	    {
            	    // InternalFormula.g:107:5: (lv_tests_3_1= ruleTest | lv_tests_3_2= ruleAllSolver )
            	    int alt2=2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0==11) ) {
            	        alt2=1;
            	    }
            	    else if ( (LA2_0==14) ) {
            	        alt2=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 2, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt2) {
            	        case 1 :
            	            // InternalFormula.g:108:6: lv_tests_3_1= ruleTest
            	            {

            	            						newCompositeNode(grammarAccess.getSolveAccess().getTestsTestParserRuleCall_2_0_0());
            	            					
            	            pushFollow(FOLLOW_3);
            	            lv_tests_3_1=ruleTest();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getSolveRule());
            	            						}
            	            						add(
            	            							current,
            	            							"tests",
            	            							lv_tests_3_1,
            	            							"org.xtext.example.mydsl.Formula.Test");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 2 :
            	            // InternalFormula.g:124:6: lv_tests_3_2= ruleAllSolver
            	            {

            	            						newCompositeNode(grammarAccess.getSolveAccess().getTestsAllSolverParserRuleCall_2_0_1());
            	            					
            	            pushFollow(FOLLOW_3);
            	            lv_tests_3_2=ruleAllSolver();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getSolveRule());
            	            						}
            	            						add(
            	            							current,
            	            							"tests",
            	            							lv_tests_3_2,
            	            							"org.xtext.example.mydsl.Formula.AllSolver");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolve"


    // $ANTLR start "entryRuleTest"
    // InternalFormula.g:146:1: entryRuleTest returns [EObject current=null] : iv_ruleTest= ruleTest EOF ;
    public final EObject entryRuleTest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTest = null;


        try {
            // InternalFormula.g:146:45: (iv_ruleTest= ruleTest EOF )
            // InternalFormula.g:147:2: iv_ruleTest= ruleTest EOF
            {
             newCompositeNode(grammarAccess.getTestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTest=ruleTest();

            state._fsp--;

             current =iv_ruleTest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTest"


    // $ANTLR start "ruleTest"
    // InternalFormula.g:153:1: ruleTest returns [EObject current=null] : (otherlv_0= 'Test' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= 'with' ( (lv_solver_3_0= ruleSatsolver ) ) otherlv_4= ';' ) ;
    public final EObject ruleTest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_formula_1_0 = null;

        EObject lv_solver_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:159:2: ( (otherlv_0= 'Test' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= 'with' ( (lv_solver_3_0= ruleSatsolver ) ) otherlv_4= ';' ) )
            // InternalFormula.g:160:2: (otherlv_0= 'Test' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= 'with' ( (lv_solver_3_0= ruleSatsolver ) ) otherlv_4= ';' )
            {
            // InternalFormula.g:160:2: (otherlv_0= 'Test' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= 'with' ( (lv_solver_3_0= ruleSatsolver ) ) otherlv_4= ';' )
            // InternalFormula.g:161:3: otherlv_0= 'Test' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= 'with' ( (lv_solver_3_0= ruleSatsolver ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getTestAccess().getTestKeyword_0());
            		
            // InternalFormula.g:165:3: ( (lv_formula_1_0= ruleEquiv ) )
            // InternalFormula.g:166:4: (lv_formula_1_0= ruleEquiv )
            {
            // InternalFormula.g:166:4: (lv_formula_1_0= ruleEquiv )
            // InternalFormula.g:167:5: lv_formula_1_0= ruleEquiv
            {

            					newCompositeNode(grammarAccess.getTestAccess().getFormulaEquivParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_5);
            lv_formula_1_0=ruleEquiv();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTestRule());
            					}
            					set(
            						current,
            						"formula",
            						lv_formula_1_0,
            						"org.xtext.example.mydsl.Formula.Equiv");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getTestAccess().getWithKeyword_2());
            		
            // InternalFormula.g:188:3: ( (lv_solver_3_0= ruleSatsolver ) )
            // InternalFormula.g:189:4: (lv_solver_3_0= ruleSatsolver )
            {
            // InternalFormula.g:189:4: (lv_solver_3_0= ruleSatsolver )
            // InternalFormula.g:190:5: lv_solver_3_0= ruleSatsolver
            {

            					newCompositeNode(grammarAccess.getTestAccess().getSolverSatsolverParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_solver_3_0=ruleSatsolver();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTestRule());
            					}
            					set(
            						current,
            						"solver",
            						lv_solver_3_0,
            						"org.xtext.example.mydsl.Formula.Satsolver");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getTestAccess().getSemicolonKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTest"


    // $ANTLR start "entryRuleAllSolver"
    // InternalFormula.g:215:1: entryRuleAllSolver returns [EObject current=null] : iv_ruleAllSolver= ruleAllSolver EOF ;
    public final EObject entryRuleAllSolver() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAllSolver = null;


        try {
            // InternalFormula.g:215:50: (iv_ruleAllSolver= ruleAllSolver EOF )
            // InternalFormula.g:216:2: iv_ruleAllSolver= ruleAllSolver EOF
            {
             newCompositeNode(grammarAccess.getAllSolverRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAllSolver=ruleAllSolver();

            state._fsp--;

             current =iv_ruleAllSolver; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAllSolver"


    // $ANTLR start "ruleAllSolver"
    // InternalFormula.g:222:1: ruleAllSolver returns [EObject current=null] : (otherlv_0= 'AllSolver' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= ';' ) ;
    public final EObject ruleAllSolver() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_formula_1_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:228:2: ( (otherlv_0= 'AllSolver' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= ';' ) )
            // InternalFormula.g:229:2: (otherlv_0= 'AllSolver' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= ';' )
            {
            // InternalFormula.g:229:2: (otherlv_0= 'AllSolver' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= ';' )
            // InternalFormula.g:230:3: otherlv_0= 'AllSolver' ( (lv_formula_1_0= ruleEquiv ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAllSolverAccess().getAllSolverKeyword_0());
            		
            // InternalFormula.g:234:3: ( (lv_formula_1_0= ruleEquiv ) )
            // InternalFormula.g:235:4: (lv_formula_1_0= ruleEquiv )
            {
            // InternalFormula.g:235:4: (lv_formula_1_0= ruleEquiv )
            // InternalFormula.g:236:5: lv_formula_1_0= ruleEquiv
            {

            					newCompositeNode(grammarAccess.getAllSolverAccess().getFormulaEquivParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_7);
            lv_formula_1_0=ruleEquiv();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAllSolverRule());
            					}
            					set(
            						current,
            						"formula",
            						lv_formula_1_0,
            						"org.xtext.example.mydsl.Formula.Equiv");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getAllSolverAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAllSolver"


    // $ANTLR start "entryRuleSatsolver"
    // InternalFormula.g:261:1: entryRuleSatsolver returns [EObject current=null] : iv_ruleSatsolver= ruleSatsolver EOF ;
    public final EObject entryRuleSatsolver() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSatsolver = null;


        try {
            // InternalFormula.g:261:50: (iv_ruleSatsolver= ruleSatsolver EOF )
            // InternalFormula.g:262:2: iv_ruleSatsolver= ruleSatsolver EOF
            {
             newCompositeNode(grammarAccess.getSatsolverRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSatsolver=ruleSatsolver();

            state._fsp--;

             current =iv_ruleSatsolver; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSatsolver"


    // $ANTLR start "ruleSatsolver"
    // InternalFormula.g:268:1: ruleSatsolver returns [EObject current=null] : ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) ) ;
    public final EObject ruleSatsolver() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalFormula.g:274:2: ( ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) ) )
            // InternalFormula.g:275:2: ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) )
            {
            // InternalFormula.g:275:2: ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            else if ( (LA4_0==16) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalFormula.g:276:3: ( (lv_name_0_0= 'SAT4J' ) )
                    {
                    // InternalFormula.g:276:3: ( (lv_name_0_0= 'SAT4J' ) )
                    // InternalFormula.g:277:4: (lv_name_0_0= 'SAT4J' )
                    {
                    // InternalFormula.g:277:4: (lv_name_0_0= 'SAT4J' )
                    // InternalFormula.g:278:5: lv_name_0_0= 'SAT4J'
                    {
                    lv_name_0_0=(Token)match(input,15,FOLLOW_2); 

                    					newLeafNode(lv_name_0_0, grammarAccess.getSatsolverAccess().getNameSAT4JKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSatsolverRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_0, "SAT4J");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:291:3: ( (lv_name_1_0= 'minisat' ) )
                    {
                    // InternalFormula.g:291:3: ( (lv_name_1_0= 'minisat' ) )
                    // InternalFormula.g:292:4: (lv_name_1_0= 'minisat' )
                    {
                    // InternalFormula.g:292:4: (lv_name_1_0= 'minisat' )
                    // InternalFormula.g:293:5: lv_name_1_0= 'minisat'
                    {
                    lv_name_1_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getSatsolverAccess().getNameMinisatKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSatsolverRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_1_0, "minisat");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSatsolver"


    // $ANTLR start "entryRuleEquiv"
    // InternalFormula.g:309:1: entryRuleEquiv returns [EObject current=null] : iv_ruleEquiv= ruleEquiv EOF ;
    public final EObject entryRuleEquiv() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquiv = null;


        try {
            // InternalFormula.g:309:46: (iv_ruleEquiv= ruleEquiv EOF )
            // InternalFormula.g:310:2: iv_ruleEquiv= ruleEquiv EOF
            {
             newCompositeNode(grammarAccess.getEquivRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEquiv=ruleEquiv();

            state._fsp--;

             current =iv_ruleEquiv; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquiv"


    // $ANTLR start "ruleEquiv"
    // InternalFormula.g:316:1: ruleEquiv returns [EObject current=null] : (this_Implie_0= ruleImplie ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )* ) ;
    public final EObject ruleEquiv() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Implie_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:322:2: ( (this_Implie_0= ruleImplie ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )* ) )
            // InternalFormula.g:323:2: (this_Implie_0= ruleImplie ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )* )
            {
            // InternalFormula.g:323:2: (this_Implie_0= ruleImplie ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )* )
            // InternalFormula.g:324:3: this_Implie_0= ruleImplie ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )*
            {

            			newCompositeNode(grammarAccess.getEquivAccess().getImplieParserRuleCall_0());
            		
            pushFollow(FOLLOW_8);
            this_Implie_0=ruleImplie();

            state._fsp--;


            			current = this_Implie_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:332:3: ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalFormula.g:333:4: () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplie ) )
            	    {
            	    // InternalFormula.g:333:4: ()
            	    // InternalFormula.g:334:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getEquivAccess().getEquivLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,17,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getEquivAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalFormula.g:344:4: ( (lv_right_3_0= ruleImplie ) )
            	    // InternalFormula.g:345:5: (lv_right_3_0= ruleImplie )
            	    {
            	    // InternalFormula.g:345:5: (lv_right_3_0= ruleImplie )
            	    // InternalFormula.g:346:6: lv_right_3_0= ruleImplie
            	    {

            	    						newCompositeNode(grammarAccess.getEquivAccess().getRightImplieParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_right_3_0=ruleImplie();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getEquivRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.Formula.Implie");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquiv"


    // $ANTLR start "entryRuleImplie"
    // InternalFormula.g:368:1: entryRuleImplie returns [EObject current=null] : iv_ruleImplie= ruleImplie EOF ;
    public final EObject entryRuleImplie() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplie = null;


        try {
            // InternalFormula.g:368:47: (iv_ruleImplie= ruleImplie EOF )
            // InternalFormula.g:369:2: iv_ruleImplie= ruleImplie EOF
            {
             newCompositeNode(grammarAccess.getImplieRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplie=ruleImplie();

            state._fsp--;

             current =iv_ruleImplie; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplie"


    // $ANTLR start "ruleImplie"
    // InternalFormula.g:375:1: ruleImplie returns [EObject current=null] : (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleImplie() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:381:2: ( (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalFormula.g:382:2: (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalFormula.g:382:2: (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalFormula.g:383:3: this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getImplieAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:391:3: ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalFormula.g:392:4: () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalFormula.g:392:4: ()
            	    // InternalFormula.g:393:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImplieAccess().getImplieLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,18,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getImplieAccess().getEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalFormula.g:403:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalFormula.g:404:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalFormula.g:404:5: (lv_right_3_0= ruleOr )
            	    // InternalFormula.g:405:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getImplieAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImplieRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.Formula.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplie"


    // $ANTLR start "entryRuleOr"
    // InternalFormula.g:427:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalFormula.g:427:43: (iv_ruleOr= ruleOr EOF )
            // InternalFormula.g:428:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalFormula.g:434:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:440:2: ( (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )* ) )
            // InternalFormula.g:441:2: (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )* )
            {
            // InternalFormula.g:441:2: (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )* )
            // InternalFormula.g:442:3: this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_10);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:450:3: ( () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalFormula.g:451:4: () otherlv_2= 'or' ( (lv_right_3_0= ruleNor ) )
            	    {
            	    // InternalFormula.g:451:4: ()
            	    // InternalFormula.g:452:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,19,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getOrKeyword_1_1());
            	    			
            	    // InternalFormula.g:462:4: ( (lv_right_3_0= ruleNor ) )
            	    // InternalFormula.g:463:5: (lv_right_3_0= ruleNor )
            	    {
            	    // InternalFormula.g:463:5: (lv_right_3_0= ruleNor )
            	    // InternalFormula.g:464:6: lv_right_3_0= ruleNor
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightNorParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleNor();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.Formula.Nor");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleNor"
    // InternalFormula.g:486:1: entryRuleNor returns [EObject current=null] : iv_ruleNor= ruleNor EOF ;
    public final EObject entryRuleNor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNor = null;


        try {
            // InternalFormula.g:486:44: (iv_ruleNor= ruleNor EOF )
            // InternalFormula.g:487:2: iv_ruleNor= ruleNor EOF
            {
             newCompositeNode(grammarAccess.getNorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNor=ruleNor();

            state._fsp--;

             current =iv_ruleNor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNor"


    // $ANTLR start "ruleNor"
    // InternalFormula.g:493:1: ruleNor returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleNor() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:499:2: ( (this_And_0= ruleAnd ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalFormula.g:500:2: (this_And_0= ruleAnd ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalFormula.g:500:2: (this_And_0= ruleAnd ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalFormula.g:501:3: this_And_0= ruleAnd ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getNorAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:509:3: ( () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalFormula.g:510:4: () otherlv_2= 'nor' ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalFormula.g:510:4: ()
            	    // InternalFormula.g:511:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getNorAccess().getNorLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,20,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getNorAccess().getNorKeyword_1_1());
            	    			
            	    // InternalFormula.g:521:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalFormula.g:522:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalFormula.g:522:5: (lv_right_3_0= ruleAnd )
            	    // InternalFormula.g:523:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getNorAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getNorRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.Formula.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNor"


    // $ANTLR start "entryRuleAnd"
    // InternalFormula.g:545:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalFormula.g:545:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalFormula.g:546:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalFormula.g:552:1: ruleAnd returns [EObject current=null] : (this_Nand_0= ruleNand ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Nand_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:558:2: ( (this_Nand_0= ruleNand ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )* ) )
            // InternalFormula.g:559:2: (this_Nand_0= ruleNand ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )* )
            {
            // InternalFormula.g:559:2: (this_Nand_0= ruleNand ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )* )
            // InternalFormula.g:560:3: this_Nand_0= ruleNand ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getNandParserRuleCall_0());
            		
            pushFollow(FOLLOW_12);
            this_Nand_0=ruleNand();

            state._fsp--;


            			current = this_Nand_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:568:3: ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==21) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalFormula.g:569:4: () otherlv_2= 'and' ( (lv_right_3_0= ruleNand ) )
            	    {
            	    // InternalFormula.g:569:4: ()
            	    // InternalFormula.g:570:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,21,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndAccess().getAndKeyword_1_1());
            	    			
            	    // InternalFormula.g:580:4: ( (lv_right_3_0= ruleNand ) )
            	    // InternalFormula.g:581:5: (lv_right_3_0= ruleNand )
            	    {
            	    // InternalFormula.g:581:5: (lv_right_3_0= ruleNand )
            	    // InternalFormula.g:582:6: lv_right_3_0= ruleNand
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightNandParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_right_3_0=ruleNand();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.Formula.Nand");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNand"
    // InternalFormula.g:604:1: entryRuleNand returns [EObject current=null] : iv_ruleNand= ruleNand EOF ;
    public final EObject entryRuleNand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNand = null;


        try {
            // InternalFormula.g:604:45: (iv_ruleNand= ruleNand EOF )
            // InternalFormula.g:605:2: iv_ruleNand= ruleNand EOF
            {
             newCompositeNode(grammarAccess.getNandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNand=ruleNand();

            state._fsp--;

             current =iv_ruleNand; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNand"


    // $ANTLR start "ruleNand"
    // InternalFormula.g:611:1: ruleNand returns [EObject current=null] : ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* ) ;
    public final EObject ruleNand() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        EObject this_Not_0 = null;

        EObject this_Primary_1 = null;

        EObject lv_right_4_1 = null;

        EObject lv_right_4_2 = null;



        	enterRule();

        try {
            // InternalFormula.g:617:2: ( ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* ) )
            // InternalFormula.g:618:2: ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* )
            {
            // InternalFormula.g:618:2: ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* )
            // InternalFormula.g:619:3: (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )*
            {
            // InternalFormula.g:619:3: (this_Not_0= ruleNot | this_Primary_1= rulePrimary )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==23) ) {
                alt10=1;
            }
            else if ( (LA10_0==RULE_ID||(LA10_0>=24 && LA10_0<=26)) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalFormula.g:620:4: this_Not_0= ruleNot
                    {

                    				newCompositeNode(grammarAccess.getNandAccess().getNotParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_13);
                    this_Not_0=ruleNot();

                    state._fsp--;


                    				current = this_Not_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalFormula.g:629:4: this_Primary_1= rulePrimary
                    {

                    				newCompositeNode(grammarAccess.getNandAccess().getPrimaryParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_13);
                    this_Primary_1=rulePrimary();

                    state._fsp--;


                    				current = this_Primary_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalFormula.g:638:3: ( () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==22) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalFormula.g:639:4: () otherlv_3= 'nand' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) )
            	    {
            	    // InternalFormula.g:639:4: ()
            	    // InternalFormula.g:640:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getNandAccess().getNandLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_3=(Token)match(input,22,FOLLOW_4); 

            	    				newLeafNode(otherlv_3, grammarAccess.getNandAccess().getNandKeyword_1_1());
            	    			
            	    // InternalFormula.g:650:4: ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) )
            	    // InternalFormula.g:651:5: ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) )
            	    {
            	    // InternalFormula.g:651:5: ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) )
            	    // InternalFormula.g:652:6: (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary )
            	    {
            	    // InternalFormula.g:652:6: (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary )
            	    int alt11=2;
            	    int LA11_0 = input.LA(1);

            	    if ( (LA11_0==23) ) {
            	        alt11=1;
            	    }
            	    else if ( (LA11_0==RULE_ID||(LA11_0>=24 && LA11_0<=26)) ) {
            	        alt11=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 11, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt11) {
            	        case 1 :
            	            // InternalFormula.g:653:7: lv_right_4_1= ruleNot
            	            {

            	            							newCompositeNode(grammarAccess.getNandAccess().getRightNotParserRuleCall_1_2_0_0());
            	            						
            	            pushFollow(FOLLOW_13);
            	            lv_right_4_1=ruleNot();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getNandRule());
            	            							}
            	            							set(
            	            								current,
            	            								"right",
            	            								lv_right_4_1,
            	            								"org.xtext.example.mydsl.Formula.Not");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalFormula.g:669:7: lv_right_4_2= rulePrimary
            	            {

            	            							newCompositeNode(grammarAccess.getNandAccess().getRightPrimaryParserRuleCall_1_2_0_1());
            	            						
            	            pushFollow(FOLLOW_13);
            	            lv_right_4_2=rulePrimary();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getNandRule());
            	            							}
            	            							set(
            	            								current,
            	            								"right",
            	            								lv_right_4_2,
            	            								"org.xtext.example.mydsl.Formula.Primary");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNand"


    // $ANTLR start "entryRuleNot"
    // InternalFormula.g:692:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalFormula.g:692:44: (iv_ruleNot= ruleNot EOF )
            // InternalFormula.g:693:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalFormula.g:699:1: ruleNot returns [EObject current=null] : (otherlv_0= '!' this_Primary_1= rulePrimary () ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_Primary_1 = null;



        	enterRule();

        try {
            // InternalFormula.g:705:2: ( (otherlv_0= '!' this_Primary_1= rulePrimary () ) )
            // InternalFormula.g:706:2: (otherlv_0= '!' this_Primary_1= rulePrimary () )
            {
            // InternalFormula.g:706:2: (otherlv_0= '!' this_Primary_1= rulePrimary () )
            // InternalFormula.g:707:3: otherlv_0= '!' this_Primary_1= rulePrimary ()
            {
            otherlv_0=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getNotAccess().getExclamationMarkKeyword_0());
            		

            			newCompositeNode(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1());
            		
            pushFollow(FOLLOW_2);
            this_Primary_1=rulePrimary();

            state._fsp--;


            			current = this_Primary_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:719:3: ()
            // InternalFormula.g:720:4: 
            {

            				current = forceCreateModelElementAndSet(
            					grammarAccess.getNotAccess().getNotExpAction_2(),
            					current);
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalFormula.g:730:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalFormula.g:730:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalFormula.g:731:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalFormula.g:737:1: rulePrimary returns [EObject current=null] : (this_Atomic_0= ruleAtomic | this_PFormula_1= rulePFormula ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        EObject this_Atomic_0 = null;

        EObject this_PFormula_1 = null;



        	enterRule();

        try {
            // InternalFormula.g:743:2: ( (this_Atomic_0= ruleAtomic | this_PFormula_1= rulePFormula ) )
            // InternalFormula.g:744:2: (this_Atomic_0= ruleAtomic | this_PFormula_1= rulePFormula )
            {
            // InternalFormula.g:744:2: (this_Atomic_0= ruleAtomic | this_PFormula_1= rulePFormula )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID||(LA13_0>=24 && LA13_0<=25)) ) {
                alt13=1;
            }
            else if ( (LA13_0==26) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalFormula.g:745:3: this_Atomic_0= ruleAtomic
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Atomic_0=ruleAtomic();

                    state._fsp--;


                    			current = this_Atomic_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalFormula.g:754:3: this_PFormula_1= rulePFormula
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_PFormula_1=rulePFormula();

                    state._fsp--;


                    			current = this_PFormula_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // InternalFormula.g:766:1: entryRuleAtomic returns [EObject current=null] : iv_ruleAtomic= ruleAtomic EOF ;
    public final EObject entryRuleAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomic = null;


        try {
            // InternalFormula.g:766:47: (iv_ruleAtomic= ruleAtomic EOF )
            // InternalFormula.g:767:2: iv_ruleAtomic= ruleAtomic EOF
            {
             newCompositeNode(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAtomic=ruleAtomic();

            state._fsp--;

             current =iv_ruleAtomic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalFormula.g:773:1: ruleAtomic returns [EObject current=null] : ( ( () ( (lv_id_1_0= RULE_ID ) ) ) | ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) ) ) ;
    public final EObject ruleAtomic() throws RecognitionException {
        EObject current = null;

        Token lv_id_1_0=null;
        Token lv_value_3_1=null;
        Token lv_value_3_2=null;


        	enterRule();

        try {
            // InternalFormula.g:779:2: ( ( ( () ( (lv_id_1_0= RULE_ID ) ) ) | ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) ) ) )
            // InternalFormula.g:780:2: ( ( () ( (lv_id_1_0= RULE_ID ) ) ) | ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) ) )
            {
            // InternalFormula.g:780:2: ( ( () ( (lv_id_1_0= RULE_ID ) ) ) | ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_ID) ) {
                alt15=1;
            }
            else if ( ((LA15_0>=24 && LA15_0<=25)) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalFormula.g:781:3: ( () ( (lv_id_1_0= RULE_ID ) ) )
                    {
                    // InternalFormula.g:781:3: ( () ( (lv_id_1_0= RULE_ID ) ) )
                    // InternalFormula.g:782:4: () ( (lv_id_1_0= RULE_ID ) )
                    {
                    // InternalFormula.g:782:4: ()
                    // InternalFormula.g:783:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getVarPropAction_0_0(),
                    						current);
                    				

                    }

                    // InternalFormula.g:789:4: ( (lv_id_1_0= RULE_ID ) )
                    // InternalFormula.g:790:5: (lv_id_1_0= RULE_ID )
                    {
                    // InternalFormula.g:790:5: (lv_id_1_0= RULE_ID )
                    // InternalFormula.g:791:6: lv_id_1_0= RULE_ID
                    {
                    lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_id_1_0, grammarAccess.getAtomicAccess().getIdIDTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"id",
                    							lv_id_1_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:809:3: ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) )
                    {
                    // InternalFormula.g:809:3: ( () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) ) )
                    // InternalFormula.g:810:4: () ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) )
                    {
                    // InternalFormula.g:810:4: ()
                    // InternalFormula.g:811:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getBoolConstantAction_1_0(),
                    						current);
                    				

                    }

                    // InternalFormula.g:817:4: ( ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) ) )
                    // InternalFormula.g:818:5: ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) )
                    {
                    // InternalFormula.g:818:5: ( (lv_value_3_1= 'true' | lv_value_3_2= 'false' ) )
                    // InternalFormula.g:819:6: (lv_value_3_1= 'true' | lv_value_3_2= 'false' )
                    {
                    // InternalFormula.g:819:6: (lv_value_3_1= 'true' | lv_value_3_2= 'false' )
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==24) ) {
                        alt14=1;
                    }
                    else if ( (LA14_0==25) ) {
                        alt14=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 0, input);

                        throw nvae;
                    }
                    switch (alt14) {
                        case 1 :
                            // InternalFormula.g:820:7: lv_value_3_1= 'true'
                            {
                            lv_value_3_1=(Token)match(input,24,FOLLOW_2); 

                            							newLeafNode(lv_value_3_1, grammarAccess.getAtomicAccess().getValueTrueKeyword_1_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAtomicRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_3_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalFormula.g:831:7: lv_value_3_2= 'false'
                            {
                            lv_value_3_2=(Token)match(input,25,FOLLOW_2); 

                            							newLeafNode(lv_value_3_2, grammarAccess.getAtomicAccess().getValueFalseKeyword_1_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getAtomicRule());
                            							}
                            							setWithLastConsumed(current, "value", lv_value_3_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRulePFormula"
    // InternalFormula.g:849:1: entryRulePFormula returns [EObject current=null] : iv_rulePFormula= rulePFormula EOF ;
    public final EObject entryRulePFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePFormula = null;


        try {
            // InternalFormula.g:849:49: (iv_rulePFormula= rulePFormula EOF )
            // InternalFormula.g:850:2: iv_rulePFormula= rulePFormula EOF
            {
             newCompositeNode(grammarAccess.getPFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePFormula=rulePFormula();

            state._fsp--;

             current =iv_rulePFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePFormula"


    // $ANTLR start "rulePFormula"
    // InternalFormula.g:856:1: rulePFormula returns [EObject current=null] : (otherlv_0= '(' this_Equiv_1= ruleEquiv () otherlv_3= ')' ) ;
    public final EObject rulePFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject this_Equiv_1 = null;



        	enterRule();

        try {
            // InternalFormula.g:862:2: ( (otherlv_0= '(' this_Equiv_1= ruleEquiv () otherlv_3= ')' ) )
            // InternalFormula.g:863:2: (otherlv_0= '(' this_Equiv_1= ruleEquiv () otherlv_3= ')' )
            {
            // InternalFormula.g:863:2: (otherlv_0= '(' this_Equiv_1= ruleEquiv () otherlv_3= ')' )
            // InternalFormula.g:864:3: otherlv_0= '(' this_Equiv_1= ruleEquiv () otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0());
            		

            			newCompositeNode(grammarAccess.getPFormulaAccess().getEquivParserRuleCall_1());
            		
            pushFollow(FOLLOW_14);
            this_Equiv_1=ruleEquiv();

            state._fsp--;


            			current = this_Equiv_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:876:3: ()
            // InternalFormula.g:877:4: 
            {

            				current = forceCreateModelElementAndSet(
            					grammarAccess.getPFormulaAccess().getPFormulaExpAction_2(),
            					current);
            			

            }

            otherlv_3=(Token)match(input,27,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePFormula"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000007800010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000008000000L});

}