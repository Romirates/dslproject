/*
 * generated by Xtext 2.15.0
 */
package org.xtext.example.mydsl.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;
import org.xtext.example.mydsl.ide.contentassist.antlr.internal.InternalFormulaParser;
import org.xtext.example.mydsl.services.FormulaGrammarAccess;

public class FormulaParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(FormulaGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, FormulaGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getSolveAccess().getAlternatives_0(), "rule__Solve__Alternatives_0");
			builder.put(grammarAccess.getSolveAccess().getTestsAlternatives_2_0(), "rule__Solve__TestsAlternatives_2_0");
			builder.put(grammarAccess.getSatsolverAccess().getAlternatives(), "rule__Satsolver__Alternatives");
			builder.put(grammarAccess.getNandAccess().getAlternatives_0(), "rule__Nand__Alternatives_0");
			builder.put(grammarAccess.getNandAccess().getRightAlternatives_1_2_0(), "rule__Nand__RightAlternatives_1_2_0");
			builder.put(grammarAccess.getPrimaryAccess().getAlternatives(), "rule__Primary__Alternatives");
			builder.put(grammarAccess.getAtomicAccess().getAlternatives(), "rule__Atomic__Alternatives");
			builder.put(grammarAccess.getAtomicAccess().getValueAlternatives_1_1_0(), "rule__Atomic__ValueAlternatives_1_1_0");
			builder.put(grammarAccess.getSolveAccess().getGroup(), "rule__Solve__Group__0");
			builder.put(grammarAccess.getTestAccess().getGroup(), "rule__Test__Group__0");
			builder.put(grammarAccess.getAllSolverAccess().getGroup(), "rule__AllSolver__Group__0");
			builder.put(grammarAccess.getEquivAccess().getGroup(), "rule__Equiv__Group__0");
			builder.put(grammarAccess.getEquivAccess().getGroup_1(), "rule__Equiv__Group_1__0");
			builder.put(grammarAccess.getImplieAccess().getGroup(), "rule__Implie__Group__0");
			builder.put(grammarAccess.getImplieAccess().getGroup_1(), "rule__Implie__Group_1__0");
			builder.put(grammarAccess.getOrAccess().getGroup(), "rule__Or__Group__0");
			builder.put(grammarAccess.getOrAccess().getGroup_1(), "rule__Or__Group_1__0");
			builder.put(grammarAccess.getNorAccess().getGroup(), "rule__Nor__Group__0");
			builder.put(grammarAccess.getNorAccess().getGroup_1(), "rule__Nor__Group_1__0");
			builder.put(grammarAccess.getAndAccess().getGroup(), "rule__And__Group__0");
			builder.put(grammarAccess.getAndAccess().getGroup_1(), "rule__And__Group_1__0");
			builder.put(grammarAccess.getNandAccess().getGroup(), "rule__Nand__Group__0");
			builder.put(grammarAccess.getNandAccess().getGroup_1(), "rule__Nand__Group_1__0");
			builder.put(grammarAccess.getNotAccess().getGroup(), "rule__Not__Group__0");
			builder.put(grammarAccess.getAtomicAccess().getGroup_0(), "rule__Atomic__Group_0__0");
			builder.put(grammarAccess.getAtomicAccess().getGroup_1(), "rule__Atomic__Group_1__0");
			builder.put(grammarAccess.getPFormulaAccess().getGroup(), "rule__PFormula__Group__0");
			builder.put(grammarAccess.getSolveAccess().getTestsAssignment_2(), "rule__Solve__TestsAssignment_2");
			builder.put(grammarAccess.getTestAccess().getFormulaAssignment_1(), "rule__Test__FormulaAssignment_1");
			builder.put(grammarAccess.getTestAccess().getSolverAssignment_3(), "rule__Test__SolverAssignment_3");
			builder.put(grammarAccess.getAllSolverAccess().getFormulaAssignment_1(), "rule__AllSolver__FormulaAssignment_1");
			builder.put(grammarAccess.getSatsolverAccess().getNameAssignment_0(), "rule__Satsolver__NameAssignment_0");
			builder.put(grammarAccess.getSatsolverAccess().getNameAssignment_1(), "rule__Satsolver__NameAssignment_1");
			builder.put(grammarAccess.getEquivAccess().getRightAssignment_1_2(), "rule__Equiv__RightAssignment_1_2");
			builder.put(grammarAccess.getImplieAccess().getRightAssignment_1_2(), "rule__Implie__RightAssignment_1_2");
			builder.put(grammarAccess.getOrAccess().getRightAssignment_1_2(), "rule__Or__RightAssignment_1_2");
			builder.put(grammarAccess.getNorAccess().getRightAssignment_1_2(), "rule__Nor__RightAssignment_1_2");
			builder.put(grammarAccess.getAndAccess().getRightAssignment_1_2(), "rule__And__RightAssignment_1_2");
			builder.put(grammarAccess.getNandAccess().getRightAssignment_1_2(), "rule__Nand__RightAssignment_1_2");
			builder.put(grammarAccess.getAtomicAccess().getIdAssignment_0_1(), "rule__Atomic__IdAssignment_0_1");
			builder.put(grammarAccess.getAtomicAccess().getValueAssignment_1_1(), "rule__Atomic__ValueAssignment_1_1");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private FormulaGrammarAccess grammarAccess;

	@Override
	protected InternalFormulaParser createParser() {
		InternalFormulaParser result = new InternalFormulaParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public FormulaGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(FormulaGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
