package org.xtext.example.mydsl
import org.sat4j.minisat.SolverFactory
import org.sat4j.reader.DimacsReader
import java.io.ByteArrayInputStream
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException


class SAT4j extends Solver {
	override test(String file) {
		val solver = SolverFactory.newDefault
		solver.setTimeout(3600); // 1 hour timeout
		val reader = new DimacsReader(solver)
		try {
			val problem = reader.parseInstance(new ByteArrayInputStream(file.getBytes("US-ASCII")));
	        if (problem.isSatisfiable()) {
	            true
	        } else {
	            false
	        }			
		} catch (ContradictionException e) {
            false
        } catch (TimeoutException e) {
            false
        }
	}
	
}