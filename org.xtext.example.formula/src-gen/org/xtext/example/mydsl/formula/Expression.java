/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.mydsl.formula;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.mydsl.formula.FormulaPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
