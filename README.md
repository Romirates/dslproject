
# DSL project:

  What is it?
  -----------

  The goal of this project is to develop a domain-specific language (DSL) 
for automatically reasoning about a satisfiability (SAT) formula. 

  The Latest Version
  ------------------

    Details of the latest version can be found on the gitlab's repository
    page under https://gitlab.com/Romirates/dslproject/.

  Documentation

  -------------
  The entry point of the code generation, is located in  
  org.xtext.example.formula/src/org/xtext/example/mydsl/
  generator/FormulaGenerator.xtend
  
  Grammar, solver interfaces are located in 
  org.xtext.example.formula/src/org/xtext/example/mydsl
  
  This implementation use equisatifiability of Tseitin transformation,
  DSL can check if a formula is satifiable given a solver,
  or if all solvers give equivalent results on a given sat problem.
  
  User can write multiple problems in a row.

  Installation
  ------------

  Licensing
  ---------
  
  Authors
  ---------
    Romain Ferrand